#include "power_manager.h"
#include "pin.h"
#include "system.h"

PowerManager::PowerManager(uint8_t in_port, uint8_t in_pin, uint8_t out_port, uint8_t out_pin)
:in_pin(in_port, in_pin), out_pin(out_port, out_pin)
{
//	vSemaphoreCreateBinary( xSoftKeyOffSemaphore);
//	xSemaphoreTake( xSoftKeyOffSemaphore,(portTickType)1);
	this->in_pin.switchToInputPU();
//	this->in_pin.setCallback(this);
//	this->in_pin.enableExti(true);
	this->out_pin.reset();
	this->out_pin.switchToOutput();


}


void PowerManager::on(uint8_t waiting_time){

	vTaskDelay(configTICK_RATE_HZ*waiting_time);
	if (!in_pin.isSet())
	{
		out_pin.set();
		power_on=true;
	}

}



void PowerManager::off(void){
	power_on=false;
	out_pin.reset();
	while(true){
		vTaskDelay(configTICK_RATE_HZ);
		IWDG_ReloadCounter();
	};

}



void PowerManager::timer_tick(void){
 static portBASE_TYPE xHigherPriorityTaskWoken;
 tevents_source_type event_sourse;
 xHigherPriorityTaskWoken = pdFALSE;
static volatile int key_press_cntr=0;
static volatile int inc=0;
if(!power_on) return;
if (in_pin.isSet())
	{
	 	 key_press_cntr=0;
	 	 inc=1;
	}
else{
		if(inc==1)
		{
			key_press_cntr+=inc;
			if(key_press_cntr>=3*50)
			{
				inc=0;


				event_sourse = SOFT_KEY_OFF;

					 				xQueueSendToBackFromISR(System::xEventQueue, &event_sourse, &xHigherPriorityTaskWoken);

					 				if( xHigherPriorityTaskWoken == pdTRUE )
					 							                          {
					 							                                portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
					 							                          }


			}
		}
	}
}



/*
void PowerManager::pin_irq_callback(void){
volatile int i;
i++;
}
*/
