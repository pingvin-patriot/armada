################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/_write.c \
../src/bitmap.c \
../src/bitmaps.c \
../src/chipher.c \
../src/hd44780_driver.c \
../src/mcu-status.c \
../src/si4432.c \
../src/ws2812b.c 

CPP_SRCS += \
../src/adc.cpp \
../src/bluetooth.cpp \
../src/display.cpp \
../src/flash.cpp \
../src/ir.cpp \
../src/keyboard.cpp \
../src/lsl.cpp \
../src/main.cpp \
../src/parser.cpp \
../src/pin.cpp \
../src/statistics.cpp \
../src/system.cpp \
../src/usart.cpp \
../src/vm.cpp \
../src/w25x.cpp \
../src/wavplayer.cpp 

OBJS += \
./src/_write.o \
./src/adc.o \
./src/bitmap.o \
./src/bitmaps.o \
./src/bluetooth.o \
./src/chipher.o \
./src/display.o \
./src/flash.o \
./src/hd44780_driver.o \
./src/ir.o \
./src/keyboard.o \
./src/lsl.o \
./src/main.o \
./src/mcu-status.o \
./src/parser.o \
./src/pin.o \
./src/si4432.o \
./src/statistics.o \
./src/system.o \
./src/usart.o \
./src/vm.o \
./src/w25x.o \
./src/wavplayer.o \
./src/ws2812b.o 

C_DEPS += \
./src/_write.d \
./src/bitmap.d \
./src/bitmaps.d \
./src/chipher.d \
./src/hd44780_driver.d \
./src/mcu-status.d \
./src/si4432.d \
./src/ws2812b.d 

CPP_DEPS += \
./src/adc.d \
./src/bluetooth.d \
./src/display.d \
./src/flash.d \
./src/ir.d \
./src/keyboard.d \
./src/lsl.d \
./src/main.d \
./src/parser.d \
./src/pin.d \
./src/statistics.d \
./src/system.d \
./src/usart.d \
./src/vm.d \
./src/w25x.d \
./src/wavplayer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\OS_WRAPPERS\inc" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\FreeRTOS\include" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\FreeRTOS\include" -I"../OS_WRAPPERS/inc" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


