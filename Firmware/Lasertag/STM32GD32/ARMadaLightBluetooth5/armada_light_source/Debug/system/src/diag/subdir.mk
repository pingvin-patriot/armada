################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../system/src/diag/Trace.c \
../system/src/diag/trace_impl.c 

OBJS += \
./system/src/diag/Trace.o \
./system/src/diag/trace_impl.o 

C_DEPS += \
./system/src/diag/Trace.d \
./system/src/diag/trace_impl.d 


# Each subdirectory must supply rules for building sources it contributes
system/src/diag/%.o: ../system/src/diag/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\OS_WRAPPERS\inc" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\FreeRTOS\include" -I"C:\RepositoryArvadaLasertagCom\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


