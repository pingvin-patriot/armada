/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------



//#include "Timer.h"
//#include "BlinkLed.h"
#include <map>
#include <string>

#ifdef __cplusplus
 extern "C" {
  #endif
#include "../FreeRTOS/include/FreeRTOS.h"
#include "../FreeRTOS/include/task.h"
#include "../FreeRTOS/include/queue.h"
#include "../FreeRTOS/include/semphr.h"
#include "ws2812b.h"

#ifdef __cplusplus
  }
#endif



// ----------------------------------------------------------------------------
//
// Standalone STM32F1 led blink sample (trace via DEBUG).
//
// In debug configurations, demonstrate how to print a greeting message
// on the trace device. In release configurations the message is
// simply discarded.
//
// Then demonstrates how to blink a led with 1 Hz, using a
// continuous loop and SysTick delays.
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//
// The external clock frequency is specified as a preprocessor definition
// passed to the compiler via a command line option (see the 'C/C++ General' ->
// 'Paths and Symbols' -> the 'Symbols' tab, if you want to change it).
// The value selected during project creation was HSE_VALUE=8000000.
//
// Note: The default clock settings take the user defined HSE_VALUE and try
// to reach the maximum possible system clock. For the default 8 MHz input
// the result is guaranteed, but for other values it might not be possible,
// so please adjust the PLL settings in system/src/cmsis/system_stm32f10x.c
//

// Definitions visible only within this translation unit.
namespace
{
  // ----- Timing definitions -------------------------------------------------

  // Keep the LED on for 2/3 of a second.
//  constexpr Timer::ticks_t BLINK_ON_TICKS = Timer::FREQUENCY_HZ * 3 / 4;
 // constexpr Timer::ticks_t BLINK_OFF_TICKS = Timer::FREQUENCY_HZ
 //     - BLINK_ON_TICKS;
}

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "diag/Trace.h"
#include "keyboard.h"
#include "wavplayer.h"
#include "ir.h"
#include "adc.h"

#include <pin.h>
#include "bluetooth.h"
#include "system.h"
#include "w25x.h"
#include "wavplayer.h"
#include <si4432.hpp>
#include <rf_modul.hpp>
#include <spi.hpp>
//#include "ili9163.hpp"
//#include "uart.hpp"
#include "statistics.h"

#ifdef __cplusplus
 extern "C" {
  #endif

// Подключаем FreeRTOS
#include "FreeRTOS.h"
// из всех возможностей используем пока только переключение задач
#include "task.h"
//#include "hd44780_driver.h"

 #ifdef __cplusplus
   }
 #endif
/*
extern unsigned char MSG1[];
extern const unsigned char MSG2[];
extern const unsigned char MSG3[];
extern const unsigned char MSG4[];



 extern const unsigned char CGRAM1[];
 extern const unsigned char CGRAM2[];
 extern const unsigned char CGRAM3[];
*/
#define NUM_LEDS    4
 RGB_t leds[NUM_LEDS];


 const unsigned char MSG1[]="Winstar ";
 const unsigned char MSG2[]="display ";
 const unsigned char MSG3[]="OLED SPI";
 const unsigned char MSG4[]="WEH0802A";


 volatile uint32_t id = 0;
 volatile uint8_t sr;

//uint8_t tx_buff[8] = {'A','R','M','a','d','a','!','!'};
//uint8_t rx_buff[256];

   void vLedTask (void *pvParameters);
   void testCallback(void);



	//Keyboard keyboard;
//	WavPlayer wavPlayer;
	//IrTransceiver ir;
	//ADC adc;
   //Si4432 si4432(PORTC,PIN5,PORTC,PIN15,PORTB,PIN0);
  // ILI9163<Spi_3> color_display(PORTB,PIN9,PORTB,PIN8,PORTB,PIN7, PORTC, PIN10);



#ifdef rf_enable
   RfModul<Spi_1> rf_modul(PORTC,PIN5,PORTC,PIN15,PORTB,PIN0);
#endif
   	System sys;




   	//  	Uart uart;



 //  SSD1283<Spi_3> disp(PORTB,PIN9,PORTB,PIN8,PORTB,PIN7, PORTC, PIN10);


int

main(int argc, char* argv[])
{

	//Pin pin1(PORTB,PIN10);
	//pin1.switchToOutput();

	//Bluetooth::statePinEnable();


		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//		AFIO->MAPR|=AFIO_MAPR_SWJ_CFG_JTAGDISABLE; //��������� JTAG, SWD �������
		AFIO->MAPR|= AFIO_MAPR_SWJ_CFG_DISABLE;/*!< JTAG-DP Disabled and SW-DP Disabled */

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); //��� ���� �





	//	lcd_set_state(LCD_ENABLE, CURSOR_ENABLE, BLINK); //�������� ������ � �������


	//init_lcd();

//	Delay1s();
//	lcd_puts("555");
	//CGRAM();
	//lcd_clrscr();

	//Delay(15);
	//lcd_puts("LTAscet");
	//lcd_gotoxy(0, 1);
	//lcd_puts("Russia");
	//lcd_gpio_init();

//	 Initial_IC();
	// CGRAM();
//	 WriteSerialData(8,(unsigned char*)MSG1);
	 // WriteString(8,(unsigned char*)MSG1);

	/*
	std::string myString;

	volatile int b;


	myString ="Hello!";
	myString = std::to_string(0x55);
	b = atoi(myString.c_str());

	myMap[0]=33;
	myMap[1]=22;
	if(myMap.at(1)==33){
		myMap[0]=35;
	}
	*/

		init_SPI2();
		//sys.load_settings();

		SPI_FLASH_CS_H();
		SPI_FLASH_CS_L();
		SPI_FLASH_CS_H();
		uint8_t reg1;

			reg1 = (uint8_t) SPI_Flash_ReadSR();
			reg1 &=0b11100011;
			SPI_Flash_WriteSR(reg1);

/*

			StatisticsItem stat_itm_tmp;
			volatile uint16_t first_free_cell;

			erase_statistics();
			for (uint16_t i=0; i<588; i++){
				stat_itm_tmp.marker = 0x12;
				stat_itm_tmp.player_id = i;
				 write_stat_item(stat_itm_tmp, i);
			}

			first_free_cell = get_first_free_cell_recursive(0,1023);

*/

			/*
			reg1 = (uint8_t) SPI_Flash_ReadSR();
			//SPI_FLASH_Write_Enable();
			reg2 = (uint8_t) SPI_Flash_ReadSR2();
			reg3 = (uint8_t) SPI_Flash_ReadSR3();

			SPI_FLASH_Write_Enable();
			reg1 = (uint8_t) SPI_Flash_ReadSR();
			*/


	/*

			SPI_Flash_Read(rx_buff,(uint32_t)0x00, 256);
			uint32_t chip_ID=0;
			chip_ID=SPI_Flash_read_ID();
	*/





			/*
			sr = SPI_Flash_ReadSR();
id = SPI_Flash_read_ID();
SPI_Flash_Erase_Sector(0x00);

		sr = SPI_Flash_ReadSR();
//SPI_FLASH_Write_Disable();
SPI_Flash_Read(rx_buff,255,8);

SPI_Flash_Write_Page( tx_buff,0,8);
//SPI_FLASH_Write_Disable();
SPI_Flash_Read_With_DMA(rx_buff,0,256);

SPI_Flash_Read(rx_buff,0,256);

SPI_Flash_Write_Page( tx_buff,10,8);

SPI_Flash_Read_With_DMA(rx_buff,0,256);







*/
			unsigned char tx_buff[8]={'H','e','l','l','o','!','!','!'};

//rf_modul.TxBuf(tx_buff, 8);

	//		lcd_alternative_init();





//	xTaskCreate(vLedTask,/*(signed char*)*/"LedTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1,  ( xTaskHandle * ) NULL);


	//wavPlayer.initTask();

	vTaskStartScheduler();


	//ws0010_spi_gpio_init();

		   // Send a greeting to the trace device (skipped on Release).
  //trace_puts("Hello ARM World!");

  // At this stage the system clock should have already been configured
  // at high speed.
 // trace_printf("System clock: %u Hz\n", SystemCoreClock);

 // Timer timer;
 // timer.start();

 // BlinkLed blinkLed;

  // Perform all necessary initialisations for the LED.
//  blinkLed.powerUp();

 // uint32_t seconds = 0;

  // Infinite loop
  while (1)
    {
/*
	  WRONG_LED_HIGH;
	  Delay1s();
	  Delay1s();
	  Delay1s();
	  Delay1s();
	  Delay1s();
	  WRONG_LED_LOW;
	  lcd_clrscr();
	 //	lcd_gotoxy(0, 0);
	  lcd_set_xy(0,0); //��������� ������ � ������ ������
	  //lcd_out("Ascet");
	  lcd_puts(" ARMada ");
//	  lcd_home();
	  // CGRAM();
	//  WriteSerialData(8,(unsigned char*)MSG1);
	  Delay1s();
	  Delay1s();
	  Delay1s();
	  Delay1s();
	  Delay1s();
	 // lcd_gotoxy(0, 1);
	//  lcd_clrscr();
	  lcd_set_xy(0,1);
	  lcd_out(" Russia ");
//	  lcd_home();
//	  WriteSerialData(8,(unsigned char*)MSG2);
  //    blinkLed.turnOn();
 //     timer.sleep(seconds== 0 ? Timer::FREQUENCY_HZ : BLINK_ON_TICKS);
//
  //    blinkLed.turnOff();
  //    timer.sleep(BLINK_OFF_TICKS);

  //    ++seconds;

      // Count seconds on the trace device.
 //     trace_printf("Second %u\n", seconds);
*/
    }
  // Infinite loop, never return.

}



void vLedTask (void *pvParameters){
/*
	uint8_t user_char[8]; //���� ����� ���������� ���������������� ������
uint8_t snd=0;
int adc_value=0;
int voltage = 0;
*/



//std::map<int,int> myMap;

//Led SCKpin(PORTB,PIN3);
//Led MOSIpin(PORTB,PIN5);
//Led SCEpin(PORTB,PIN9);
////sys.bluetooth.reset();

/*
sys.display->power_off();
vTaskDelay(configTICK_RATE_HZ);
sys.display->power_on();
*/

//taskENTER_CRITICAL();
//sys.display.init();
/*
vTaskDelay(configTICK_RATE_HZ);
lcd_alternative_init();


sys.load_settings();
*/

/*
vTaskDelay(1200);
	sys.bluetooth->configure();
vTaskDelay(600);
*/
//color_display.init();

//sys.bluetooth->switch_to_data_mode();
//taskEXIT_CRITICAL();
//lcd_init(); //�������������� �������




/*
user_char[0]=0b01110; //� ��� ���
user_char[1]=0b10001; // ������
user_char[2]=0b10001; // ��� ������
user_char[3]=0b10001; //
user_char[4]=0b10001; // ��� ���� ���� :-)
user_char[5]=0b01010;
user_char[6]=0b10001;
user_char[7]=0b10001;
//lcd_set_user_char(0, user_char); // ���� ���� ��� ������ ����� ����
sys.display->set_user_char(0,user_char);
//	lcd_out((char*)"Fish"); //������ ������� � ������� ������
sys.display->print((char*)"Fish");
//	lcd_set_xy(0,1); //��������� ������ � ������ ������
sys.display->go_to_xy(0,1);
//lcd_send(0,DATA); //������ ������ ����� ����
sys.display->print_user_char(0,DATA);
*/





/*
vTaskDelay(configTICK_RATE_HZ*2);
lcd_generate_additional_symbols();
*/
/*
lcd_set_xy(7,0);

sys.display.print_user_char(0,DATA);
*/
/*
sys.display->init_gui();
vTaskDelay(configTICK_RATE_HZ*2);
*/

//lcd_alternative_init();

//// sys.display.init();

/*
sys.ir.set_player_id(10);
sys.ir.set_team_color(Red);
sys.ir.set_gun_damage(Damage_1);
*/


//ws0010_spi_gpio_init();
//WS0010_SCK_LOW;
//	 WS0010_MOSI_LOW;
//	 WS0010_SCE_LOW;
/*
ws0010_mosi_high();
ws0010_mosi_low();
ws0010_sck_high();
ws0010_sck_low();
ws0010_sce_high();
ws0010_sce_low();
*/
//ws0010_init();
//lcd_puts("Hello!");
//ws0010_spi_read();

//Led BtPin(PORTB,PIN1);
//BtPin.on();
//BtPin.off();
//Led CePin(PORTB,PIN9);
/*
Pin BtStatePin(PORTC,PIN7);
BtStatePin.enableExti(true);
BtStatePin.setCallback(testCallback);

Pin BtResetPin(PORTC,PIN4);
BtResetPin.switchToOutput();
BtResetPin.set();
                vTaskDelay(100);
BtResetPin.reset();
                vTaskDelay(100);
BtResetPin.set();


myMap[0]=33;
	myMap[1]=22;
	if(myMap.at(1)==22){
		myMap[0]=35;
	}
*/
//BtPin.switchToOutput();
//int *pI;
//pI = new int;
//*pI=1;

 //char  symbols[2]={0,0};

	for (;;)
    {

/*
		disp.init();
		disp.SCE.reset();
		disp.SetAddr(0,0,129,129);
		for (int i=0; i<129*129;i++){
		                		disp.SB(0x0000+i, Dat);
		                	}

		                	disp.spi.wait_for_bus_free();

		                	disp.SCE.set();


		 vTaskDelay(600);
*/
 //       GPIO_SetBits(GPIOA, GPIO_Pin_0);
		// WRONG_LED_HIGH;


		/*



		vTaskDelay(configTICK_RATE_HZ/2);

	//	taskENTER_CRITICAL();
		//lcd_clear();

		lcd_set_xy(0,0); //��������� ������ � ������ ������
               	  ////lcd_out("Ascet");
               //	  lcd_out(" ARMada ");
		 sys.display->print((char*)"ARMada");


	*/


		 //    taskEXIT_CRITICAL();
     //          	  bluetooth.SendStr("ARMada\r\n");

                /*
                if(sys.bluetooth.connectStatus())
               	  {
               		  //lcd_out((char*)"Connect");
               		  sys.display.print((char*)"Connect");
               	  }
               	  else sys.display.print((char*)"DisConn");

                adc_value = sys.adc.value();
               	voltage = sys.adc.voltage();
               	*/


/*
               	leds[0].r=(uint8_t)255;
               	leds[0].g=0;
               	leds[0].b=0;
               	while (!ws2812b_IsReady()); // wait
               	ws2812b_SendRGB(leds, NUM_LEDS);
               	sys.bluetooth->PutChar('h');
  */



              // 	lcd_send(symbols[0],DATA);
               	// symbols[0]+=1;
 //               GPIO_ResetBits(GPIOA, GPIO_Pin_0);
  //              BtPin.on();
            //    CePin.on();
    //            SCKpin.on();
      //          MOSIpin.on();
        //        SCEpin.on();
 //               lcd_home();
 //               lcd_puts("Hello!");
/*
               	if (snd<61) {
               		snd++;
               		WavPlayer::add_sound_to_play(snd++);
                   	vTaskDelay(100);
               		WavPlayer::play_sound_now(snd);
                //   	WavPlayer::stop();
               	}

*/
     //          	vTaskDelay(600);
  //             	taskENTER_CRITICAL();




    /*
               	lcd_set_xy(0,0);
             //   lcd_out((char*)" Russia ");
                sys.display->print((char*)"Russia");
    //            taskEXIT_CRITICAL();
        		vTaskDelay(configTICK_RATE_HZ/2);
        		sys.display->update_battary_voltage();

      */





 //       		lcd_clear();
/*
        		lcd_set_xy(0,0);

        		sys.display.print_user_char(0,DATA);
        		sys.display.print_user_char(1,DATA);
        		sys.display.print_user_char(2,DATA);
        		sys.display.print_user_char(3,DATA);
        		sys.display.print_user_char(4,DATA);
        		sys.display.print_user_char(5,DATA);
        		vTaskDelay(configTICK_RATE_HZ*2);
        		lcd_clear();

*/
         /*
                if (sys.keyboard.triggerIsPressed()){
                	//lcd_out((char*)"Pressed");
                	sys.display.print((char*)"Pressed");
                }
                else {
                	sys.display.print((char*)"NotPress");
               }
*/
                /*ir.low_power();
                ir.send_shot_package();
                	  //lcd_out(" Russia ");
                	//  WRONG_LED_LOW;
                	  vTaskDelay(900);
                ir.hight_power();
                ir.send_shot_package();
               	*/
		/*
                leds[0].r=0;
               	leds[0].g=255;
               	leds[0].b=0;
               	while (!ws2812b_IsReady()); // wait
               	ws2812b_SendRGB(leds, NUM_LEDS);
               	sys.bluetooth->PutChar('U');
               	vTaskDelay(600);

          */

       //           lcd_clear();
     //           SCKpin.off();
       //         MOSIpin.off();
         //       SCEpin.off();

           //     BtPin.off();
              //  CePin.off();

    }
 //   vTaskDelete(NULL);
}


void testCallback(void){
static volatile int x=0;
x++;

}


#ifdef __cplusplus
 extern "C" {
  #endif
void DMA2_Channel2_IRQHandler(void)
{
	ILI9163<Spi_3>::dma_iqr_collback();
}
#ifdef __cplusplus
  }
#endif




#ifdef __cplusplus
extern "C" {
  #endif
void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
    configASSERT( NULL );
}
#ifdef __cplusplus
  }
#endif



#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
