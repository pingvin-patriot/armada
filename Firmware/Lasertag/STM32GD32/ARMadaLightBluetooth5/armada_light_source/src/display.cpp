
#include <bitmaps.h>
#include "display.h"
#include "system.h"
#ifdef __cplusplus
 extern "C" {
  #endif

#include "cipher.h"
#include "bitmap.h"
#include "ws2812b.h"
#ifdef __cplusplus
  }
#endif







//extern const uint16_t gerb_pic[0x4000];
extern	System sys;



const uint32_t* lic_key = (uint32_t*) 0x807F800;




OledDisplay::OledDisplay():power_pin(PORTC,PIN10){
//	init();
power_pin.set();
power_pin.switchToOutputOD();
}

xSemaphoreHandle ColorDisplay::xAnimationTaskSemaphore;

ColorDisplay::ColorDisplay(uint8_t SCE_PORT, uint8_t SCE_PIN,
		uint8_t RST_PORT, uint8_t RST_PIN,
		uint8_t DC_PORT, uint8_t DC_PIN,
		uint8_t BL_PORT, uint8_t BL_PIN):color_display
				(SCE_PORT, SCE_PIN,RST_PORT, RST_PIN,
				DC_PORT, DC_PIN,BL_PORT, BL_PIN){
	 self_pointer = this;
	vSemaphoreCreateBinary(xAnimationTaskSemaphore);
	xSemaphoreTake(xAnimationTaskSemaphore,(portTickType)1);
	xTaskCreate( vAnimationTask,/* ( signed char * )*/ "AnimationTask", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2,
	                            ( xTaskHandle * ) NULL);


}


void ColorDisplay::init(){
	color_display.init();
}


void ColorDisplay::print (char* str){

}

void ColorDisplay::go_to_xy(uint8_t x, uint8_t y){

}


void ColorDisplay::update_battary_voltage(void){
	static const uint16_t* batt_picters[6] = {
			batt_long_level_0,
			batt_long_level_1,
			batt_long_level_2,
			batt_long_level_3,
			batt_long_level_4,
			batt_long_level_5
	};
	volatile TextParamStruct TS;
		uint16_t mV_in_step;
		int16_t x;
	static volatile	uint16_t* ppic; //указатель на картинку
	uint8_t integer,tenths,hundredths;
	uint16_t voltage;
	voltage = (uint16_t)sys.adc->voltage();




	integer = voltage/1000;//целая часть
	tenths = (voltage - integer*1000)/100;//десятые
	hundredths = (voltage - integer*1000 - tenths*100)/10;//сотые
	mV_in_step = ( FULL_VOLTAGE - LOW_VOLTAGE)/5;
	x = (voltage - LOW_VOLTAGE)/mV_in_step;
	if (x<0)//разряжена
	{
		ppic = (uint16_t*)batt_picters[0];//покажем пустую батарею
	}
	else {

		//if(x==0)ppic = batt_picters[1];
		if (x<5) ppic =  (uint16_t*)batt_picters[x+1];
		else ppic = (uint16_t*) batt_picters[5];

	}

	TS.Size = 0;
	TS.Font = StdFont;
	TS.XPos = batt_text_x_pos;
	TS.YPos = batt_text_y_pos;
	TS.TxtCol = iGreen;//iRed;
	TS.BkgCol = color_display.BKGCol;
	TS.Precision = 2;
	TS.Padding = 0;

	color_display.spi.wait_for_bus_free();
	color_display.spi.init();




	if(color_display.type==Ordinary)
	{
		color_display.SB(0x36, Reg);//Set Memory access mode
		if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
		else color_display.SB((0x08 /*|(1<<7)*/), Dat);

		color_display.PNum((int32_t)integer,(TextParamStruct *) &TS);
		color_display.PStr(".",  (TextParamStruct *)&TS);
		color_display.PNum((int32_t)tenths,(TextParamStruct *) &TS);
		color_display.PNum((int32_t)hundredths, (TextParamStruct *)&TS);
		color_display.PStr("V", (TextParamStruct *)&TS);

		color_display.drawBMPfromFlashUseDMA((uint16_t *)ppic,65,0,20,64,Rotation_90);

	}

	else{

		color_display.PNum((int32_t)integer,(TextParamStruct *) &TS);
				color_display.PStr(".",  (TextParamStruct *)&TS);
				color_display.PNum((int32_t)tenths,(TextParamStruct *) &TS);
				color_display.PNum((int32_t)hundredths, (TextParamStruct *)&TS);
				color_display.PStr("V", (TextParamStruct *)&TS);

				color_display.drawBMPfromFlashUseDMA((uint16_t *)ppic,65,0,20,64,Rotation_0);

	}







}

void ColorDisplay::update_health(void){
	volatile TextParamStruct TS;
	TS.Size = 2;
	TS.Font = StdFont;
	TS.XPos = health_text_x_pos;
	TS.YPos = health_text_y_pos;
	TS.TxtCol = iWhite;//iGreen;//iRed;
	TS.BkgCol = color_display.BKGCol;
	TS.Precision = 2;
	TS.Padding = 2;

	color_display.spi.wait_for_bus_free();
	color_display.spi.init();
	color_display.SB(0x36, Reg);//Set Memory access mode

	if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
	else color_display.SB((0x08 /*|(1<<7)*/), Dat);
	color_display.PNum((int32_t)sys.settings.player.health, (TextParamStruct *)&TS);

}

void ColorDisplay::update_rounds(void){
	volatile TextParamStruct TS;
	TS.Size = 1;
	TS.Font = StdFont;
	TS.XPos = rounds_text_x_pos;
	TS.YPos = rounds_text_y_pos;
	TS.TxtCol = iGreen;//iRed;
	TS.BkgCol = color_display.BKGCol;
	TS.Precision = 2;
	TS.Padding = 2;

	color_display.spi.wait_for_bus_free();
	color_display.spi.init();
	color_display.SB(0x36, Reg);//Set Memory access mode

	if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
	else color_display.SB((0x08 /*|(1<<7)*/), Dat);
	color_display.PNum((int32_t)sys.settings.gun.rounds, (TextParamStruct *)&TS);
}

void ColorDisplay::update_clips(void){
	volatile TextParamStruct TS;
	TS.Size = 1;
	TS.Font = StdFont;
	TS.XPos = clips_text_x_pos;
	TS.YPos = clips_text_y_pos;
	TS.TxtCol = iWhite;//iGreen;//iRed;
	TS.BkgCol = color_display.BKGCol;//iBlack;//iWhite;
	TS.Precision = 2;
	TS.Padding = 2;

	color_display.spi.wait_for_bus_free();
	color_display.spi.init();
	color_display.SB(0x36, Reg);//Set Memory access mode
	if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
	else color_display.SB((0x08 /*|(1<<7)*/), Dat);
	color_display.PNum((int32_t)sys.settings.gun.clips,(TextParamStruct *) &TS);


}

void ColorDisplay::update_bluetooth(void){

	if(!gui_already_init) return;
	if(sys.bluetooth->connectStatus())
		{
			init_gui(sys.in_game());
			//show_bluetooth_icon();

		}
	else
		{
			stop_animation();
			show_guns_picture();
			//hide_bluetooth_icon();
			//sys.matryoshka_enable = false;
		}

}

void ColorDisplay::update_fire_mode(void){
	volatile TextParamStruct TS;

	TS.Size = 0;
	TS.Font = StdFont;
	TS.XPos =  fire_mode_text_x_pos;
	TS.YPos = fire_mode_text_y_pos;
	TS.TxtCol = iWhite;//iGreen;//iRed;
	TS.BkgCol = color_display.BKGCol;


	color_display.spi.wait_for_bus_free();
		color_display.spi.init();
		color_display.SB(0x36, Reg);//Set Memory access mode
		if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
		else color_display.SB((0x08 /*|(1<<7)*/), Dat);
		switch(sys.settings.gun.fire_mode){
					case SINGLE:
					{
						color_display.PStr("SING", (TextParamStruct *)&TS);
					}
					break;
					case SEMI_AUTO:
					{
						color_display.PStr("SEMI", (TextParamStruct *)&TS);

					}
					break;
					case AUTO:
					{
						color_display.PStr("AUTO",(TextParamStruct *)&TS);
					}
					break;
					default:break;
					}
}

void ColorDisplay::init_gui(bool game_status){

//	taskENTER_CRITICAL();

	gui_already_init = true;
	if (!sys.bluetooth->connectStatus()) {

		stop_animation();
		show_guns_picture();

	//	taskEXIT_CRITICAL();
		return;
	}
	//taskEXIT_CRITICAL();

	if (!game_status){
		stop_animation();
		 show_game_over_picture();
		 show_bluetooth_icon();
		 return;

	}



	color_display.ClrScrn();
	if(color_display.type==Ordinary)
		{
	color_display.drawBMPfromFlashUseDMA((uint16_t*)bullet,64,40,64,21,Rotation_90);
	color_display.drawBMPfromFlashUseDMA((uint16_t*)clip_2,93,40,35,64,Rotation_0);
		}
	else{
		color_display.drawBMPfromFlashUseDMA((uint16_t*)bullet,64,40,64,21,Rotation_0);
		color_display.drawBMPfromFlashUseDMA((uint16_t*)clip_2,24,93,35,64,Rotation_90);
	}

	//show_heart_picture(0);
	start_animation();
	update_health();
	update_clips();
	update_rounds();

	taskENTER_CRITICAL();
	update_fire_mode();
	taskEXIT_CRITICAL();

	//update_bluetooth();
	if(sys.bluetooth->connectStatus())
			{

				show_bluetooth_icon();

			}
		else
			{

				//hide_bluetooth_icon();
			}

}

#define rounds_text_x_pos 55
#define rounds_text_y_pos 107


void ColorDisplay::show_logo(void){
/*
	static volatile TextParamStruct TS;
	TS.Size = 1;
	TS.Font = StdFont;
	TS.XPos = rounds_text_x_pos;
	TS.YPos = rounds_text_y_pos;
	TS.TxtCol = iGreen;//iRed;
	TS.BkgCol =iBlack;//iWhite;
	TS.Precision = 2;
	TS.Padding = 2;
	color_display.spi.wait_for_bus_free();
	color_display.spi.init();
	color_display.SB(0x36, Reg);//Set Memory access mode
	if(color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
	else  color_display.SB((0x08), Dat);
	 color_display.	PNum((int32_t)55, (TextParamStruct *)&TS);
*/
	if(color_display.type==Ordinary){
	color_display.drawBMPfromFlashUseDMA((uint16_t*) gerb_pic,0,0,128,128,Rotation_0);
	}
	else{
		color_display.ClrScrn();
		color_display.drawBMPfromFlashUseDMA((uint16_t*) gerb_pic,0,0,128,128,Rotation_90);
	}
	//color_display.drawBMPfromFlash((uint16_t*) gerb_pic,128,128);
}


void ColorDisplay::show_guns_picture(void){

	if(color_display.type==Ordinary){
		color_display.drawBMPfromFlashUseDMA((uint16_t*) guns,0,0,128,128,Rotation_0);
	}
	else{
		color_display.drawBMPfromFlashUseDMA((uint16_t*) guns,0,0,128,128,Rotation_90);
	}
}


void ColorDisplay::show_bluetooth_icon(void){

	color_display.drawBMPfromFlash((uint16_t *)bluetooth1,16,32);

}

void ColorDisplay::hide_bluetooth_icon(void){

	color_display.FillRect(color_display.BKGCol/*iBlack*/,0,0,16,32);
}

void ColorDisplay::show_heart_picture(uint8_t picture_index){
	if(picture_index>3) return;
	 uint16_t* pHeardPic;
	 if (picture_index==0) pHeardPic = (uint16_t*)heart_1;
	 else if (picture_index==1)pHeardPic = (uint16_t*)heart_2;
	 else if (picture_index==2)pHeardPic = (uint16_t*)heart_3;
	 else if (picture_index==3)pHeardPic = (uint16_t*)heart_4;
	 if(color_display.type==Ordinary){
	 color_display.drawBMPfromFlashUseDMA(pHeardPic,heart_x_pos,heart_y_pos,55,48,Rotation_0);
	 }
	 else {
		 color_display.drawBMPfromFlashUseDMA(pHeardPic,heart_y_pos,heart_x_pos,55,48,Rotation_90);
	 }

}



void ColorDisplay::show_game_over_picture(void){

	if(color_display.type==Ordinary)
	{
		color_display.drawBMPfromFlashUseDMA((uint16_t*)game_over_pic,0,0,128,128,Rotation_0);
	}
	else {
		color_display.drawBMPfromFlashUseDMA((uint16_t*)game_over_pic,0,0,128,128,Rotation_90);
	}
}


void ColorDisplay::stop_animation(void){
	xSemaphoreTake(xAnimationTaskSemaphore, (portTickType)(configTICK_RATE_HZ/2));
};

void ColorDisplay::start_animation(void){
	curr_pic_num = 0;
	xSemaphoreGive(xAnimationTaskSemaphore);
}




uint8_t ColorDisplay::curr_pic_num = 0;
ColorDisplay* ColorDisplay::self_pointer = 0;



void ColorDisplay::vAnimationTask (void *pvParameters){
static volatile int8_t pic_num_inc = 1;
	for (;;) {

		if(xSemaphoreTake(xAnimationTaskSemaphore, (portTickType)portMAX_DELAY/*(configTICK_RATE_HZ/12)*/)== pdTRUE )
		{
			if(xSemaphoreTake( self_pointer->xDisplaySemaphore, (portTickType)(configTICK_RATE_HZ/2))== pdTRUE ){

				self_pointer->show_heart_picture(curr_pic_num);
				xSemaphoreGive(self_pointer->xDisplaySemaphore);
			}



			xSemaphoreGive(xAnimationTaskSemaphore);

		if (curr_pic_num == 0)vTaskDelay(2*configTICK_RATE_HZ/3);
		else vTaskDelay(configTICK_RATE_HZ/12);

		if (curr_pic_num>=3) {
			pic_num_inc=-1;
		}
		else if(curr_pic_num==0){
			pic_num_inc=1;
		}

		curr_pic_num+=pic_num_inc;



		}



	 }

}


extern RGB_t leds[];
void  ColorDisplay::incorrect_lic_key(void){
	volatile TextParamStruct TS;
	stop_animation();

	/*
		TS.Size = 2;
		TS.Font = StdFont;
		TS.XPos = health_text_x_pos;
		TS.YPos = health_text_y_pos;
		TS.TxtCol = iWhite;//iGreen;//iRed;
		TS.BkgCol = color_display.BKGCol;
		TS.Precision = 2;
		TS.Padding = 2;
*/
		color_display.ClrScrn();

		if(color_display.type==Ordinary)
		{
			color_display.drawBMPfromFlashUseDMA((uint16_t*) key_pic,0,64,128,64,Rotation_0);
		}
		else
		{
			color_display.drawBMPfromFlashUseDMA((uint16_t*) key_pic,0,0/*64*/,128,64,Rotation_90);
		}



		TS.Size = 1;
		TS.Font = StdFont;
		TS.XPos = 5;// fire_mode_text_x_pos;
		TS.YPos = 25;//fire_mode_text_y_pos;
		TS.TxtCol = iRed;//iWhite;//iGreen;
		TS.BkgCol = color_display.BKGCol;


		color_display.spi.wait_for_bus_free();
		color_display.spi.init();


		if(color_display.type==Ordinary)
		{
		color_display.SB(0x36, Reg);//Set Memory access mode

		if (color_display.LCD_MODUL_VERSION == 2) color_display.SB((0x08 |(1<<7)|(1<<6)), Dat);
		else color_display.SB((0x08 /*|(1<<7)*/), Dat);
		}

		color_display.PStr("NO LICENSE!",(TextParamStruct *)&TS);




		//vTaskDelay(portMAX_DELAY);

/*



<<<<<<< .mine
		}while(key_incorrect);
*/

}


const char hex_numbers[]={"0123456789ABCDEF"};

char* Display::lic_key_to_str(){
	static volatile char str[8*4+1];
	uint32_t* pNum = (uint32_t*)lic_key;
	for(int k=0; k<4; k++)
	{
		uint8_t* pChr = (uint8_t* ) &pNum[k];
		for (int i=0;i<4;i++){
		str[k*8+2*i]=hex_numbers[pChr[3-i]>>4];
		str[k*8+2*i+1]=hex_numbers[pChr[3-i]&0x0f];

		}
	}
	str[8*4] = '\0';
	return (char*)&str;
}

char* Display::hardware_key_to_str(){
	constexpr static uint32_t key_A [4] = {(uint32_t)0x8a3dee5c,(uint32_t)0xce73fbe1dd,(uint32_t)0x34f29a77,(uint32_t)0x93ace1f4};
	static volatile char str[8*4+1];
	uint32_t request_text[4];
	get_request_text(request_text);
	xtea2_encipher(64,request_text,key_A);
	for(int k=0; k<4; k++)
	{
		uint8_t* pChr = (uint8_t* ) &request_text[k];
		for (int i=0;i<4;i++){
		str[k*8+2*i]=hex_numbers[pChr[3-i]>>4];
		str[k*8+2*i+1]=hex_numbers[pChr[3-i]&0x0f];

		}
	}
	str[8*4] = '\0';
	return (char*)&str;

}



const char numbers[]={"0123456789"};

char* Display::int_to_str(uint8_t x, uint8_t digits){

	static volatile char str[6];



	volatile uint8_t celoe, ostatok;
	celoe=x;

	int digits_tmp;
	digits_tmp=digits;
	if (digits == 0) digits_tmp=3;
	      for (int i=(digits_tmp-1); i>=0; i--)
	      {

	      ostatok= celoe%10;
		  celoe  = celoe/10;
		  str[i]= numbers[ostatok];
	      }
	      str[digits_tmp]='\0';



	if (digits == 0)
	{
	        while ((str[0]=='0')&str[1] !='\0') for (int i=0; i < 6; i++) str[i]=str[i+1];
	}

	      return (char*)&str;

	}

	char* Display::int_to_str(uint16_t x, uint8_t digits){

	static volatile char str[6];



	volatile uint16_t celoe, ostatok;
	celoe=x;

	int digits_tmp;
	digits_tmp=digits;
	if (digits == 0) digits_tmp=5;
	      for (int i=(digits_tmp-1); i>=0; i--)
	      {

	      ostatok= celoe%10;
		  celoe  = celoe/10;
		  str[i]= numbers[ostatok];
	      }
	      str[digits_tmp]='\0';



	if (digits == 0)
	{
	        while ((str[0]=='0')&str[1] !='\0') for (int i=0; i < 6; i++) str[i]=str[i+1];
	}

	      return (char*)&str;

}






void OledDisplay::init(void){
	//lcd_init();
	lcd_alternative_init();
	lcd_generate_additional_symbols();
//	uint8_t user_char[8];

}

void OledDisplay::show_logo(void){
	go_to_xy(0,0);
	print((char*)"Firmware");
	go_to_xy(0,1);
	print((char*)"Ver. 0.1");

}
void OledDisplay::print(char* str){
	lcd_out(str);
}


void OledDisplay::go_to_xy(uint8_t x, uint8_t y){

	lcd_set_xy(x,y);
}


void OledDisplay::set_user_char(uint8_t char_num, uint8_t * char_data){
	lcd_set_user_char(char_num, char_data);
}

void OledDisplay::print_user_char(uint8_t byte, dat_or_comm dc){
	lcd_send(byte, dc);
}


void OledDisplay::update_battary_voltage(void){
int voltage;
uint8_t curr_batt_level; //������� ������ ��������� ������� (1 �� 6 ���������)
voltage = sys.adc->voltage();
go_to_xy(0,7);
if (voltage < LOW_VOLTAGE){
	lcd_generate_bat_level_symbols(0);
	return;
}
if (voltage > FULL_VOLTAGE){
	lcd_generate_bat_level_symbols(5);
	return;
}
curr_batt_level = (6*(voltage -LOW_VOLTAGE))/(FULL_VOLTAGE - LOW_VOLTAGE);
lcd_generate_bat_level_symbols(curr_batt_level);


}

void OledDisplay::update_health(void){
	go_to_xy(1,0);
	print((char*)int_to_str(sys.settings.player.health,3));
}


void OledDisplay::update_rounds(void){
	go_to_xy(1,1);
	print((char*)int_to_str(sys.settings.gun.rounds,3));
}

void OledDisplay::update_clips(void){
	go_to_xy(6,1);
	print((char*)int_to_str(sys.settings.gun.clips,2));
	print((char*)" ");

}

void OledDisplay::update_fire_mode(void){
	go_to_xy(5,0);

	switch(sys.settings.gun.fire_mode){
			case SINGLE:
			{
				print((char*)"S");
			}
			break;
			case SEMI_AUTO:
			{
				print((char*)"A");

			}
			break;
			case AUTO:
			{
				print((char*)"Q");
			}
			break;
			default:break;
			}

}



void OledDisplay::init_gui(bool game_status){

lcd_clear();
if (!game_status){
	go_to_xy(0,0);
	print((char*)"  GAME  ");
	go_to_xy(0,1);
	print((char*)"  OVER  ");
	return;

}

go_to_xy(0,0);
print_user_char(1,DATA);
update_health();

go_to_xy(7,0);
print_user_char(0,DATA);
update_battary_voltage();

go_to_xy(0,1);
print_user_char(2,DATA);
update_rounds();

go_to_xy(4,1);
print((char*)"/");
print_user_char(3,DATA);
update_clips();
update_fire_mode();
update_bluetooth();


}



void OledDisplay::update_bluetooth(void){

if (!sys.in_game()) return;
if(sys.bluetooth->connectStatus())
	{
		go_to_xy(6,0);
		print_user_char(6,DATA);

	}
else
	{
		go_to_xy(6,0);
		print((char*)" ");
		//sys.matryoshka_enable = true;
	}

}

void OledDisplay::incorrect_lic_key(void){
	lcd_clear();
	go_to_xy(3,0);
	print((char*)"NO");
	go_to_xy(0,1);
	print((char*)"LICENSE!");

}

