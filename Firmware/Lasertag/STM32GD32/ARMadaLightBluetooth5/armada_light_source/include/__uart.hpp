#ifndef __UART_HPP
#define __UART_HPP
#include <stdint.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"

#ifdef __cplusplus
 extern "C" {
  #endif
#include "../FreeRTOS/include/FreeRTOS.h"
#include "../FreeRTOS/include/task.h"
#include "../FreeRTOS/include/queue.h"
#include "../FreeRTOS/include/semphr.h"


#ifdef __cplusplus
  }
#endif


  constexpr static uint32_t UART_RX_SIZE_BUF = 1024;
  constexpr static uint32_t UART_TX_SIZE_BUF = 1024;


  class Uart{
  public:
	  xSemaphoreHandle xUsartSemaphore;
		bool fraw_data_mode;
		uint32_t maximum_bytes_in_buff;


	  Uart(USART_TypeDef* usart_x = USART1, uint32_t boudRate=9600){
	  	vSemaphoreCreateBinary(xUsartSemaphore);
	  	GPIO_InitTypeDef port;
	  	USART_InitTypeDef usart;
	  	pUsart = usart_x;

	  if(usart_x == USART1){
	      RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA| RCC_APB2Periph_AFIO, ENABLE);
	      //���� PA9 � PA10 � ������ �������������� ������� �
	           //Rx � Tx USART��
	          GPIO_StructInit(&port);
	          port.GPIO_Mode = GPIO_Mode_AF_PP;
	          port.GPIO_Pin = GPIO_Pin_9;
	          port.GPIO_Speed = GPIO_Speed_2MHz;
	          GPIO_Init(GPIOA, &port);


	          port.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	          port.GPIO_Pin = GPIO_Pin_10;
	          port.GPIO_Speed = GPIO_Speed_2MHz;
	          GPIO_Init(GPIOA, &port);

	          //��������� USART, ��� ���� ��������� ����������, ����� �������� ������
	          USART_StructInit(&usart);
	          usart.USART_BaudRate = boudRate;//BAUDRATE;
	          USART_Init(USART1, &usart);
	          USART_Cmd(USART1, ENABLE);
	  }
	  else if (usart_x == USART2){
	      RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
	      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA| RCC_APB2Periph_AFIO, ENABLE);

	      //���� PA2 � PA3 � ������ �������������� ������� �
	       //Rx � Tx USART��
	      GPIO_StructInit(&port);
	      port.GPIO_Mode = GPIO_Mode_AF_PP;
	      port.GPIO_Pin = GPIO_Pin_2;
	      port.GPIO_Speed = GPIO_Speed_2MHz;
	      GPIO_Init(GPIOA, &port);


	      port.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	      port.GPIO_Pin = GPIO_Pin_3;
	      port.GPIO_Speed = GPIO_Speed_2MHz;
	      GPIO_Init(GPIOA, &port);

	      USART_StructInit(&usart);
	      usart.USART_BaudRate = boudRate;//BAUDRATE;
	      USART_Init(USART2, &usart);
	      USART_Cmd(USART2, ENABLE);

	  }


	  }



  private:
		USART_TypeDef* pUsart;
		unsigned char TxBuf[UART_TX_SIZE_BUF];
		uint32_t txBufTail;
		uint32_t txBufHead;
		uint32_t txCount;


		//�������� �����
		unsigned char RxBuf[UART_RX_SIZE_BUF];
		uint32_t rxBufTail;
		uint32_t rxBufHead;
		uint32_t rxCount;

  protected:


  };



#endif
