#ifndef _ILI9163_HPP
#define ILI9163_HPP

#ifdef __cplusplus
 extern "C" {
  #endif
#include "../FreeRTOS/include/FreeRTOS.h"
#include "../FreeRTOS/include/task.h"
#include "../FreeRTOS/include/queue.h"
#include "../FreeRTOS/include/semphr.h"

#include <string.h>
#include <stdint.h>
#include "stm32f10x.h"
//#include "types.h"

#ifdef __cplusplus
  }
#endif

#include "spi.hpp"
#include "pin.h"

//#define LetterSpace 1
//#define STLen 32


 constexpr static uint8_t  LetterSpace = 1;
 constexpr static uint8_t  STLen = 32;

enum WModesS{
 	Dat,
 	Reg,
 };

using WModes = WModesS;

enum SleepModesS{
	Sleep,
	Awake,
};

using SleepModes = SleepModesS;


enum Colours16B{
	iBlack = 0x0000,
	iBlue = 0x001F,
	iRed = 0xF800,
	iGreen = 0x07E0,
	iWhite = 0xFFFF,
};

using Colours8 = Colours16B;

enum Rotation{
	Rotation_0,
	Rotation_90,
	Rotation_180,
	Rotation_270
};

using TRotation = Rotation;

typedef enum FontsS{
	StdFont,
	Font1
};

using Fonts = FontsS;


typedef enum SemicircleTypes{
	SemiU,
	SemiD
};
using SemicircleTypes = SemicircleTypes;

typedef struct TextParamStructS{
	uint8_t Size;
	Fonts Font;
	uint8_t XPos;
	uint8_t YPos;
	int8_t Padding;
	uint8_t Precision;
	Colours8 TxtCol;
	Colours8 BkgCol;
};
using TextParamStruct = TextParamStructS;


typedef struct TPixelS{
	uint8_t R;
	uint8_t G;
	uint8_t B;
};
using TPixel = TPixelS;

typedef struct ShapeParamStructS{
	FunctionalState Fill;
	Colours8 LineCol;
	Colours8 FillCol;
	uint8_t Thickness;
	uint8_t XSPos;
	uint8_t YSPos;
	uint8_t XEPos;
	uint8_t YEPos;
	uint8_t Radius;
	uint16_t Angle;
	SemicircleTypes SemiUD;
	FunctionalState Dashed;
};
using ShapeParamStruct = ShapeParamStructS;

enum DisplayType{
	 Ordinary,
	 Reflector
};

using TDisplayType = DisplayType;

const char Chars[] =
{
		0x00, 0x00, 0x00, 0x00, 0x00 // 20
		,0x00, 0x00, 0x5f, 0x00, 0x00 // 21 !
		,0x00, 0x07, 0x00, 0x07, 0x00 // 22 "
		,0x14, 0x7f, 0x14, 0x7f, 0x14 // 23 #
		,0x24, 0x2a, 0x7f, 0x2a, 0x12 // 24 $
		,0x23, 0x13, 0x08, 0x64, 0x62 // 25 %
		,0x36, 0x49, 0x55, 0x22, 0x50 // 26 &
		,0x00, 0x05, 0x03, 0x00, 0x00 // 27 '
		,0x00, 0x1c, 0x22, 0x41, 0x00 // 28 (
		,0x00, 0x41, 0x22, 0x1c, 0x00 // 29 )
		,0x14, 0x08, 0x3e, 0x08, 0x14 // 2a *
		,0x08, 0x08, 0x3e, 0x08, 0x08 // 2b +
		,0x00, 0x50, 0x30, 0x00, 0x00 // 2c ,
		,0x08, 0x08, 0x08, 0x08, 0x08 // 2d -
		,0x00, 0x60, 0x60, 0x00, 0x00 // 2e .
		,0x20, 0x10, 0x08, 0x04, 0x02 // 2f /
		,0x3e, 0x51, 0x49, 0x45, 0x3e // 30 0
		,0x00, 0x42, 0x7f, 0x40, 0x00 // 31 1
		,0x42, 0x61, 0x51, 0x49, 0x46 // 32 2
		,0x21, 0x41, 0x45, 0x4b, 0x31 // 33 3
		,0x18, 0x14, 0x12, 0x7f, 0x10 // 34 4
		,0x27, 0x45, 0x45, 0x45, 0x39 // 35 5
		,0x3c, 0x4a, 0x49, 0x49, 0x30 // 36 6
		,0x01, 0x71, 0x09, 0x05, 0x03 // 37 7
		,0x36, 0x49, 0x49, 0x49, 0x36 // 38 8
		,0x06, 0x49, 0x49, 0x29, 0x1e // 39 9
		,0x00, 0x36, 0x36, 0x00, 0x00 // 3a :
		,0x00, 0x56, 0x36, 0x00, 0x00 // 3b ;
		,0x08, 0x14, 0x22, 0x41, 0x00 // 3c <
		,0x14, 0x14, 0x14, 0x14, 0x14 // 3d =
		,0x00, 0x41, 0x22, 0x14, 0x08 // 3e >
		,0x02, 0x01, 0x51, 0x09, 0x06 // 3f ?
		,0x32, 0x49, 0x79, 0x41, 0x3e // 40 @
		,0x7e, 0x11, 0x11, 0x11, 0x7e // 41 A
		,0x7f, 0x49, 0x49, 0x49, 0x36 // 42 B
		,0x3e, 0x41, 0x41, 0x41, 0x22 // 43 C
		,0x7f, 0x41, 0x41, 0x22, 0x1c // 44 D
		,0x7f, 0x49, 0x49, 0x49, 0x41 // 45 E
		,0x7f, 0x09, 0x09, 0x09, 0x01 // 46 F
		,0x3e, 0x41, 0x49, 0x49, 0x7a // 47 G
		,0x7f, 0x08, 0x08, 0x08, 0x7f // 48 H
		,0x00, 0x41, 0x7f, 0x41, 0x00 // 49 I
		,0x20, 0x40, 0x41, 0x3f, 0x01 // 4a J
		,0x7f, 0x08, 0x14, 0x22, 0x41 // 4b K
		,0x7f, 0x40, 0x40, 0x40, 0x40 // 4c L
		,0x7f, 0x02, 0x0c, 0x02, 0x7f // 4d M
		,0x7f, 0x04, 0x08, 0x10, 0x7f // 4e N
		,0x3e, 0x41, 0x41, 0x41, 0x3e // 4f O
		,0x7f, 0x09, 0x09, 0x09, 0x06 // 50 P
		,0x3e, 0x41, 0x51, 0x21, 0x5e // 51 Q
		,0x7f, 0x09, 0x19, 0x29, 0x46 // 52 R
		,0x46, 0x49, 0x49, 0x49, 0x31 // 53 S
		,0x01, 0x01, 0x7f, 0x01, 0x01 // 54 T
		,0x3f, 0x40, 0x40, 0x40, 0x3f // 55 U
		,0x1f, 0x20, 0x40, 0x20, 0x1f // 56 V
		,0x3f, 0x40, 0x38, 0x40, 0x3f // 57 W
		,0x63, 0x14, 0x08, 0x14, 0x63 // 58 X
		,0x07, 0x08, 0x70, 0x08, 0x07 // 59 Y
		,0x61, 0x51, 0x49, 0x45, 0x43 // 5a Z
		,0x00, 0x7f, 0x41, 0x41, 0x00 // 5b [
		,0x02, 0x04, 0x08, 0x10, 0x20 // 5c
		,0x00, 0x41, 0x41, 0x7f, 0x00 // 5d ]
		,0x04, 0x02, 0x01, 0x02, 0x04 // 5e ^
		,0x40, 0x40, 0x40, 0x40, 0x40 // 5f _
		,0x00, 0x01, 0x02, 0x04, 0x00 // 60 `
		,0x20, 0x54, 0x54, 0x54, 0x78 // 61 a
		,0x7f, 0x48, 0x44, 0x44, 0x38 // 62 b
		,0x38, 0x44, 0x44, 0x44, 0x20 // 63 c
		,0x38, 0x44, 0x44, 0x48, 0x7f // 64 d
		,0x38, 0x54, 0x54, 0x54, 0x18 // 65 e
		,0x08, 0x7e, 0x09, 0x01, 0x02 // 66 f
		,0x0c, 0x52, 0x52, 0x52, 0x3e // 67 g
		,0x7f, 0x08, 0x04, 0x04, 0x78 // 68 h
		,0x00, 0x44, 0x7d, 0x40, 0x00 // 69 i
		,0x20, 0x40, 0x44, 0x3d, 0x00 // 6a j
		,0x7f, 0x10, 0x28, 0x44, 0x00 // 6b k
		,0x00, 0x41, 0x7f, 0x40, 0x00 // 6c l
		,0x7c, 0x04, 0x18, 0x04, 0x78 // 6d m
		,0x7c, 0x08, 0x04, 0x04, 0x78 // 6e n
		,0x38, 0x44, 0x44, 0x44, 0x38 // 6f o
		,0x7c, 0x14, 0x14, 0x14, 0x08 // 70 p
		,0x08, 0x14, 0x14, 0x18, 0x7c // 71 q
		,0x7c, 0x08, 0x04, 0x04, 0x08 // 72 r
		,0x48, 0x54, 0x54, 0x54, 0x20 // 73 s
		,0x04, 0x3f, 0x44, 0x40, 0x20 // 74 t
		,0x3c, 0x40, 0x40, 0x20, 0x7c // 75 u
		,0x1c, 0x20, 0x40, 0x20, 0x1c // 76 v
		,0x3c, 0x40, 0x30, 0x40, 0x3c // 77 w
		,0x44, 0x28, 0x10, 0x28, 0x44 // 78 x
		,0x0c, 0x50, 0x50, 0x50, 0x3c // 79 y
		,0x44, 0x64, 0x54, 0x4c, 0x44 // 7a z
		,0x00, 0x08, 0x36, 0x41, 0x00 // 7b {
		,0x00, 0x00, 0x7f, 0x00, 0x00 // 7c |
		,0x00, 0x41, 0x36, 0x08, 0x00 // 7d }
		,0x10, 0x08, 0x08, 0x10, 0x08 // 7e
		,0x78, 0x46, 0x41, 0x46, 0x78 // 7f
		,(char)0xFF, 0x00, 0x00, 0x00, 0x00 // Current char line
};



const int16_t ST[] =
{
		0, 799, 1567, 2275, 2896, 3405,
		3783, 4016, 4095, 4016, 3783, 3405,
		2896, 2275, 1567, 799, 0, -799,
		-1567, -2275, -2896, -3405, -3783, -4016,
		-4095, -4016, -3783, -3405, -2896, -2275,
		-1567, -799, 0, 799
};








//iBlack;

template<class SPIx>
class ILI9163 {

public:

	constexpr static bool LCD_NOT_STANDART = true;
	constexpr static uint16_t XPix = 128;
	constexpr static uint16_t YPix = 128;
	constexpr static uint8_t LCD_MODUL_VERSION = 2;
	constexpr static Colours8 BKGCol = iBlack;//iBlue;

	TDisplayType type = Ordinary;

	ILI9163(uint8_t SCE_PORT, uint8_t SCE_PIN,
			uint8_t RST_PORT, uint8_t RST_PIN,
			uint8_t DC_PORT, uint8_t DC_PIN,
			uint8_t BL_PORT, uint8_t BL_PIN
	):
		SCE(SCE_PORT, SCE_PIN),
		RST(RST_PORT, RST_PIN),
		DC(DC_PORT, DC_PIN),
		BL(BL_PORT, BL_PIN)

{
		vSemaphoreCreateBinary( xColorLCD_DMA_Semaphore);

		RST.set();
		SCE.set();
		DC.set();
		BL.set();

		SCE.switchToOutput();
		RST.switchToOutput();
		DC.switchToOutput();
		BL.switchToOutputOD();
};

void Delay(uint32_t MS){
			vTaskDelay(MS/(1000/configTICK_RATE_HZ));
		}


void reset(void){
	Delay(50);
	RST.reset();
	Delay(50);
	RST.set();
	Delay(2*50);
}

void bl_on(){
	BL.reset();
}

void bl_off(){
	BL.set();
}


//RGB222 to RGB565
uint16_t EToS(uint8_t Col){
	uint16_t Temp = 0;

	/* 8 bit
	Temp |= (Col&3)<<3;
	Temp |= ((Col>>2)&7)<<8;
	Temp |= (Col>>5)<<13;
	 */

	Temp |= (Col&3)<<3;
	Temp |= ((Col>>2)&3)<<9;
	Temp |= ((Col>>4)&3)<<14;

	return Temp;
}




void SB(uint8_t Data, uint8_t DR){
		if(DR == Dat) 	DC.set();//LCD_DC1;            // передача данных //GPIO_SetBits(GPIOA, AOPin);
		else DC.reset();//LCD_DC0;//GPIO_ResetBits(GPIOA, AOPin);

		spi.Send((uint16_t)Data);//SPI_SendData8(SPI1, Data);
	}

void SW(uint16_t Data, uint8_t DR){
	if(DR == Dat) 	DC.set();//LCD_DC1;            // передача данных //GPIO_SetBits(GPIOA, AOPin);
	else DC.reset();//LCD_DC0;//GPIO_ResetBits(GPIOA, AOPin);
	spi.Send((uint16_t)(Data>>8));//SPI_SendData8(SPI1, Data>>8);
	spi.Send((uint16_t)Data);

}

virtual void SetAddr(uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2){
	SB(0x2A, Reg);
	SB(0x00, Dat);

	//#ifdef LCD_NOT_STANDART
if(LCD_NOT_STANDART)	SB(X1+2, Dat);
//#else
else	SB(X1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)	SB(X2+2, Dat);
//#else
	else SB(X2, Dat);
//#endif

	SB(0x2B, Reg);
	SB(0x00, Dat);


//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)	SB(Y1+3, Dat);
//#else
	else SB(32+Y1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)	SB(Y2+3, Dat);
//#else
	else SB(32+Y2, Dat);
//#endif

	SB(0x2C, Reg);

}



void SetAddrForBMP(uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2){
	SB(0x2A, Reg);
	SB(0x00, Dat);
//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART) SB(X1+2, Dat);
//#else
	else SB(X1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(X2+2, Dat);
//#else
	else SB(X2, Dat);
//#endif

	SB(0x2B, Reg);
	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y1+3, Dat);
//#else
	else SB(Y1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y2+3, Dat);
//#else
	else SB(Y2, Dat);
//#endif

	SB(0x2C, Reg);
}


void SetAddrForRotate90(uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2){
	SB(0x2A, Reg);
	SB(0x00, Dat);
//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(X1+2, Dat);
//#else
	else SB(32+X1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(X2+2, Dat);
//#else
	else SB(32+X2, Dat);
//#endif

	SB(0x2B, Reg);
	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y1+3, Dat);
//#else
	else SB(Y1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y2+3, Dat);
//#else
	else SB(Y2, Dat);
//#endif

	SB(0x2C, Reg);
}


void SetScrn(Colours8 Colour){
	uint16_t XCnt, YCnt;

	SetAddr(0, 0, XPix-1, YPix-1);

	for(XCnt = 0; XCnt<XPix; XCnt++){
		for(YCnt = 0; YCnt<YPix; YCnt++){
			SW(Colour, Dat);
		}
	}
}

virtual void FillRect(Colours8 Colour, uint8_t X, uint8_t Y, uint8_t size_x, uint8_t size_y){
	uint16_t XCnt, YCnt;
	spi.wait_for_bus_free();
	spi.init();
	/*
	while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
		while (SPI3->SR & SPI_SR_BSY);
		init_spi3();
	*/
		SB(0x36, Reg); //Set Memory access mode
//#if    		LCD_MODUL_VERSION == 2
	if(LCD_MODUL_VERSION == 2)	SB((0x08  |(1<<7)|(1<<6)), Dat);
//#elif 	LCD_MODUL_VERSION == 1
	else SB((0x08 /*|(1<<7)*/), Dat);
//#endif

		SetAddr(0, 0, size_x-1, size_y-1);
		for(XCnt = 0; XCnt<size_x; XCnt++){
				for(YCnt = 0; YCnt<size_y; YCnt++){
					SW(Colour, Dat);
				}
			}

}




virtual void ClrScrn(void){
	/*
	while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
	while (SPI3->SR & SPI_SR_BSY);
	init_spi3();
	*/
	spi.wait_for_bus_free();
	spi.init();
	SB(0x36, Reg); //Set Memory access mode


//#if    		LCD_MODUL_VERSION == 2
if(LCD_MODUL_VERSION == 2)	SB((0x08 |(1<<7)|(1<<6)), Dat);
//#elif 	LCD_MODUL_VERSION == 1
else	SB((0x08 /*|(1<<7)*/), Dat);
//#endif
	SetScrn(BKGCol);
}


void WritePix(uint16_t X, uint16_t Y, Colours8 Colour){
	SetAddr(X, Y, X, Y);
	//PCol(Colour);
	SW(Colour, Dat);
}


void SleepMode(uint8_t Mode){
	if(Mode == Sleep) SB(0x10, Reg);
	else SB(0x11, Reg);

	Delay(120);
}

void InvMode(uint8_t Mode){
	if(Mode==0) SB(0x20, Reg);
	else SB(0x21, Reg);
}





void init(void){

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  ////////////// GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE); // SWD Remap (освобождаем PA15, PB3, PB4)
//   GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);

/*
   RCC_APB2PeriphClockCmd(LCD_PH, ENABLE);

	  GPIO_InitTypeDef GPIO_InitStructure;

	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	  GPIO_InitStructure.GPIO_Pin   = DC | RST | SCE ;
	  GPIO_Init(LCD_PORT, &GPIO_InitStructure);
*/


	spi.init();

//	GPIO_ResetBits(GPIOA, ResPin);
////	LCD_RST0;           // RST=0 - сброс дисплея
	reset();
/*
	RST.reset();
	Delay(20);
//	GPIO_SetBits(GPIOA, ResPin);
////	LCD_RST1;           // RST=1
	RST.set();
	Delay(20);
*/
//	GPIO_ResetBits(GPIOA, CSPin);
////	LCD_CS0;            // CS=0  - начали сеанс работы с дисплеем
	SCE.reset();
//	GPIO_SetBits(GPIOA, BLPin);
////	BL_ON;

	bl_on();

	SB(0x01, Reg); //Software reset
	SB(0x11, Reg); //Exit Sleep

	Delay(20);

	SB(0x26, Reg); //Set default gamma
	SB(0x04, Dat);

	SB(0xC0, Reg); //Set Power Control 1
	SB(0x1F, Dat);

	SB(0xC1, Reg); //Set Power Control 2
	SB(0x00, Dat);

	SB(0xC2, Reg); //Set Power Control 3
	SB(0x00, Dat);
	SB(0x07, Dat);

	SB(0xC3, Reg); //Set Power Control 4 (Idle mode)
	SB(0x00, Dat);
	SB(0x07, Dat);

	SB(0xC5, Reg); //Set VCom Control 1
	SB(0x24, Dat); // VComH = 3v
	SB(0xC8, Dat); // VComL = 0v

	SB(0x38, Reg); //Idle mode off
	//SB(0x39, Reg); //Enable idle mode

	SB(0x3A, Reg); //Set pixel mode
	SB(0x05, Dat);


	/*
	MY, MX, MV, ML,RGB, MH, D1, D0
	0 | 0 | 0 | 0 | 1 | 0 | 0 | 0 //normal
	1 | 0 | 0 | 0 | 1 | 0 | 0 | 0 //Y-Mirror
	0 | 1 | 0 | 0 | 1 | 0 | 0 | 0 //X-Mirror
	1 | 1 | 0 | 0 | 1 | 0 | 0 | 0 //X-Y-Mirror
	0 | 0 | 1 | 0 | 1 | 0 | 0 | 0 //X-Y Exchange
	1 | 0 | 1 | 0 | 1 | 0 | 0 | 0 //X-Y Exchange, Y-Mirror
	0 | 1 | 1 | 0 | 1 | 0 | 0 | 0 //XY exchange
	1 | 1 | 1 | 0 | 1 | 0 | 0 | 0
	*/


	SB(0x36, Reg); //Set Memory access mode
	SB((0x08/*|(1<<7)*/), Dat);

	SB(0x29, Reg); //Display on

	InvMode(0);
	ClrScrn();
}

void init_dma_to_color_lcd(uint16_t* pic_adress, uint32_t pic_size)//настраиваем DMA для работы с SPI2 (датчиками)
{
	DMA_InitTypeDef  DMA_to_SPI_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
	DMA_DeInit ( DMA2_Channel2 );
	DMA_to_SPI_InitStructure.DMA_PeripheralBaseAddr =(uint32_t)&(SPI3->DR);
	DMA_to_SPI_InitStructure.DMA_MemoryBaseAddr = (uint32_t)pic_adress;//&Sinus;
	DMA_to_SPI_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_to_SPI_InitStructure.DMA_BufferSize = pic_size;
	DMA_to_SPI_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_to_SPI_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_to_SPI_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;//DMA_PeripheralDataSize_Byte;
	DMA_to_SPI_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;//DMA_MemoryDataSize_Byte;
	DMA_to_SPI_InitStructure.DMA_Mode = DMA_Mode_Normal;//DMA_Mode_Circular;
	DMA_to_SPI_InitStructure.DMA_Priority = DMA_Priority_High;/*DMA_Priority_Low*/;
	DMA_to_SPI_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA2_Channel2, &DMA_to_SPI_InitStructure);
	/* Разрешаем прерывание по окончанию передачи данных */

	DMA_ITConfig(DMA2_Channel2, DMA_IT_TC , ENABLE);
//	DMA_ITConfig(DMA2_Channel2, DMA_IT_HT , ENABLE);
//	DMA_ITConfig(DMA2_Channel2, DMA_IT_TE , ENABLE);



	NVIC_SetPriority(DMA2_Channel2_IRQn, 14);
	NVIC_EnableIRQ(DMA2_Channel2_IRQn);
	DMA_Cmd(DMA2_Channel2, DISABLE);
//	while ( !DMA_GetFlagStatus ( DMA2_FLAG_TC2 ) );

}


// обновление дисплея содержимым буфера
void color_lcd_dma_refresh(uint16_t data_size) {


	SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, DISABLE);
	DMA_Cmd(DMA2_Channel2, DISABLE);
	/*
	while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
	while (SPI3->SR & SPI_SR_BSY);
*/
	spi.wait_for_bus_free();
	DMA_SetCurrDataCounter(DMA2_Channel2, data_size);
////	SPI_Cmd(SPI3, ENABLE);
	//LCD_DC1;//данные
	DC.set();
	SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, ENABLE);
	DMA_Cmd(DMA2_Channel2, ENABLE);


//	startSPI3();
/*
	unsigned char y, x;

	for (y=0;y<6;y++) for (x=0;x<84;x++) lcd8544_senddata(lcd8544_buff[y*84+x]);
*/
}


/**********************************
 * GFXC
 *
 **********************************/


uint16_t GenerateColour(uint8_t RedA, uint8_t GreenA, uint8_t BlueA){
	uint16_t TCol = 0;
	RedA>>=3;
	GreenA>>=2;
	BlueA>>=3;

	TCol |= BlueA;
	TCol |= (GreenA<<5);
	TCol |= (RedA<<11);

	return TCol;
}

void TextParamInit(TextParamStruct* T){
	T->Size = 0;
	T->Font = StdFont;
	T->XPos = 0;
	T->Padding = 0;
	T->Precision = 0;
	T->YPos = 0;
	T->TxtCol = iBlack;
	T->BkgCol = iWhite;
}



void ShapeParamInit(ShapeParamStruct* S){
	S->Fill = DISABLE;
	S->LineCol = iBlack;
	S->FillCol = iWhite;
	S->Thickness = 0;
	S->XSPos = 0;
	S->YSPos = 0;
	S->XEPos = 0;
	S->YEPos = 0;
	S->Radius = 0;
	S->SemiUD = SemiU;
	S->Dashed = ENABLE;
}



virtual int8_t PChar(uint16_t Ch, TextParamStruct *T){
	uint8_t X = T->XPos, Y = T->YPos, Size = T->Size;
	if(X>XPix-(Size+1)*5 || Y>YPix-(Size+1)*8 || Ch < 0x20) return -1;

	uint8_t XCnt, YCnt;

	Ch-=0x20;
	Ch*=5;

	switch(Size){
	case 0:
		SetAddr(X, Y, X+4+LetterSpace, Y+7);
		for(YCnt = 0; YCnt<8; YCnt++){
			for(XCnt = 0; XCnt<5+LetterSpace; XCnt++){
				//if(XCnt<5 && (Chars[Ch+XCnt] & (1<<YCnt))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<5 && (Chars[Ch+XCnt] & (1<<YCnt))) SW(T->TxtCol, Dat);
				else SW(T->BkgCol, Dat);
			}
		}
		break;
	case 1:
		SetAddr(X, Y, X+9+LetterSpace, Y+15);
		for(YCnt = 0; YCnt<16; YCnt++){
			for(XCnt = 0; XCnt<10+LetterSpace; XCnt++){
				//if(XCnt<10 && (Chars[Ch+(XCnt>>1)] & (1<<(YCnt>>1)))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<10 && (Chars[Ch+(XCnt>>1)] & (1<<(YCnt>>1)))) SW(T->TxtCol, Dat);
				else SW(T->BkgCol, Dat);
			}
		}
		break;
	case 2:
		SetAddr(X, Y, X+14+LetterSpace, Y+23);
		for(YCnt = 0; YCnt<24; YCnt++){
			for(XCnt = 0; XCnt<15+LetterSpace; XCnt++){
				//if(XCnt<15 && (Chars[Ch+((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<15 && (Chars[Ch+((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) SW(T->TxtCol, Dat);
				else SW(T->BkgCol, Dat);
			}
		}
		break;
	}

	T->XPos = X+(1+Size)*5+LetterSpace;

	return 0;
}


int8_t PStr(const char* Str, TextParamStruct *T){
	uint8_t X = T->XPos, Y = T->YPos, Size = T->Size;
	uint8_t SCnt, StrL;

	StrL = strlen(Str);

	switch(Size){
	case 0:
		if(X>XPix - StrL*(5+LetterSpace) || Y>YPix-8) return -1;

		for(SCnt = 0; SCnt<StrL; SCnt++){
			PChar(Str[SCnt], T);
		}
		break;
	case 1:
		if(X>XPix - StrL*(10+LetterSpace) || Y>YPix-16) return -1;

		for(SCnt = 0; SCnt<StrL; SCnt++){
			PChar(Str[SCnt], T);
		}
		break;
	case 2:
		if(X>XPix - StrL*(15+LetterSpace) || Y>YPix-24) return -1;

		for(SCnt = 0; SCnt<StrL; SCnt++){
			PChar(Str[SCnt], T);
		}
	}

	return 0;
}



int8_t PNum(int32_t Num, TextParamStruct *T){
	char NBuf[10];
	uint8_t Cnt, Len = 0, Sign;
	uint8_t X = T->XPos, Y = T->YPos, Size = T->Size;
	int8_t Pad = T->Padding;

	for(Cnt = 0; Cnt<10; Cnt++) NBuf[Cnt] = 0;

	if(T->XPos<0) return -2;

	if(Num<0){
		Num = -Num;
		Sign = 1;
	}
	else if(Num>=0){
		Sign = 0;
	}

	if(Num>9) Pad-=1;
	if(Num>99) Pad-=2;
	if(Num>999) Pad-=3;
	if(Num>9999) Pad-=4;
	if(Num>99999) Pad-=5;
	if(Num>999999) Pad-=6;
	if(Num>9999999) Pad-=7;

	if(Pad<0) Pad = 0;

	if(Num<10){
		NBuf[0] = Num + '0';
		Len = 1;
	}
	else if(Num<100){
		NBuf[0] = Num/10 + '0';
		NBuf[1] = Num % 10  + '0';
		Len = 2;
	}
	else if(Num<1000){
		NBuf[0] = Num/100 + '0';
		NBuf[1] = Num/10 % 10 + '0';
		NBuf[2] = Num % 10 + '0';
		Len = 3;
	}
	else if(Num<10000){
		NBuf[0] = Num/1000 + '0';
		NBuf[1] = Num/100 % 10 + '0';
		NBuf[2] = Num/10 % 10 + '0';
		NBuf[3] = Num % 10 + '0';
		Len = 4;
	}
	else if(Num<100000){
		NBuf[0] = Num/10000 + '0';
		NBuf[1] = Num/1000 % 10 + '0';
		NBuf[2] = Num/100 % 10 + '0';
		NBuf[3] = Num/10 % 10 + '0';
		NBuf[4] = Num % 10 + '0';
		Len = 5;
	}
	else if(Num<1000000){
		NBuf[0] = Num/100000 + '0';
		NBuf[1] = Num/10000 % 10 + '0';
		NBuf[2] = Num/1000 % 10 + '0';
		NBuf[3] = Num/100 % 10 + '0';
		NBuf[4] = Num/10 % 10 + '0';
		NBuf[5] = Num % 10 + '0';
		Len = 6;
	}
	else if(Num<10000000){
		NBuf[0] = Num/1000000 + '0';
		NBuf[1] = Num/100000 % 10 + '0';
		NBuf[2] = Num/10000 % 10 + '0';
		NBuf[3] = Num/1000 % 10 + '0';
		NBuf[4] = Num/100 % 10 + '0';
		NBuf[5] = Num/10 % 10 + '0';
		NBuf[6] = Num % 10 + '0';
		Len = 7;
	}
	else if(Num<100000000){
		NBuf[0] = Num/10000000 + '0';
		NBuf[1] = Num/1000000 % 10 + '0';
		NBuf[2] = Num/100000 % 10 + '0';
		NBuf[3] = Num/10000 % 10 + '0';
		NBuf[4] = Num/1000 % 10 + '0';
		NBuf[5] = Num/100 % 10 + '0';
		NBuf[6] = Num/10 % 10 + '0';
		NBuf[7] = Num % 10 + '0';
		Len = 8;
	}
	else if(Num<1000000000){
		NBuf[0] = Num/100000000 + '0';
		NBuf[1] = Num/10000000 % 10 + '0';
		NBuf[2] = Num/1000000 % 10 + '0';
		NBuf[3] = Num/100000 % 10 + '0';
		NBuf[4] = Num/10000 % 10 + '0';
		NBuf[5] = Num/1000 % 10 + '0';
		NBuf[6] = Num/100 % 10 + '0';
		NBuf[7] = Num/10 % 10 + '0';
		NBuf[8] = Num % 10 + '0';
		Len = 9;
	}

	switch(Size){
	case 0:
		if(X>XPix - Len*(5+LetterSpace) || Y>YPix-8) return -1;
		break;
	case 1:
		if(X>XPix - Len*(10+LetterSpace) || Y>YPix-16) return -1;
		break;
	case 2:
		if(X>XPix - Len*(15+LetterSpace) || Y>YPix-24) return -1;
		break;
	}

	if(Sign == 1){
		PChar('-', T);
	}
	if(Pad>0){
		for(Cnt = 0; Cnt<Pad; Cnt++){
			PChar('0', T);
		}
	}
	for(Cnt = 0; Cnt<Len; Cnt++){
		PChar(NBuf[Cnt], T);
	}

	return 0;
}


int8_t PNumF(float Num, TextParamStruct *T){
	int32_t IPart, FracI;
	float Frac;
	uint8_t Sign;

	if(Num<0){
		Sign = 1;
		Num = -Num;
	}
	else Sign = 0;

	IPart = Num;

	if(Sign) PNum(-IPart, T);
	else PNum(IPart, T);

	if(T->Precision>0){
		Frac = Num - IPart;
		switch(T->Precision){
		case 1:
			FracI = (int32_t)(Frac*10);
			break;
		case 2:
			FracI = (int32_t)(Frac*100);
			break;
		case 3:
			FracI = (int32_t)(Frac*1000);
			break;
		case 4:
			FracI = (int32_t)(Frac*10000);
			break;
		case 5:
			FracI = (int32_t)(Frac*100000);
			break;
		}

		PChar('.', T);
		PNum(FracI, T);
	}
	return 0;
}



int8_t Circle(ShapeParamStruct S){
	if((S.XSPos+S.Radius)>XPix || (S.YSPos+S.Radius)>YPix) return 1;

	int32_t X, Y, D, X2M1;
	int8_t TCnt;
	Y = S.Radius;
	D = -S.Radius;
	X2M1 = -1;

	if(S.Fill == ENABLE){
		S.Radius-=S.Thickness;
		for(Y=-S.Radius; Y<=S.Radius; Y++){
			for(X=-S.Radius; X<=S.Radius; X++){
				if(X*X+Y*Y <= S.Radius*S.Radius + ((S.Radius*205)>>8)){
					WritePix(S.XSPos+X, S.YSPos+Y, S.FillCol);
				}
			}
		}
	}

	if(S.Thickness>0){
		for(X = 0; X<S.Radius*0.7071f; X++){
			X2M1+=2;
			D+=X2M1;
			if(D>=0){
				Y--;
				D-= (Y<<1);
			}

			switch(S.Thickness){
			case 1:
				WritePix(S.XSPos+X, S.YSPos+Y, S.LineCol);
				WritePix(S.XSPos-X, S.YSPos+Y, S.LineCol);
				WritePix(S.XSPos+X, S.YSPos-Y, S.LineCol);
				WritePix(S.XSPos-X, S.YSPos-Y, S.LineCol);
				WritePix(S.XSPos+Y, S.YSPos+X, S.LineCol);
				WritePix(S.XSPos-Y, S.YSPos+X, S.LineCol);
				WritePix(S.XSPos+Y, S.YSPos-X, S.LineCol);
				WritePix(S.XSPos-Y, S.YSPos-X, S.LineCol);
				break;
			case 2:
				S.Radius+=1;
				for(TCnt = -1; TCnt<1; TCnt++){
					WritePix(S.XSPos+X+TCnt, S.YSPos+Y+TCnt, S.LineCol);
					WritePix(S.XSPos-X-TCnt, S.YSPos+Y+TCnt, S.LineCol);
					WritePix(S.XSPos+X+TCnt, S.YSPos-Y-TCnt, S.LineCol);
					WritePix(S.XSPos-X-TCnt, S.YSPos-Y-TCnt, S.LineCol);
					WritePix(S.XSPos+Y+TCnt, S.YSPos+X+TCnt, S.LineCol);
					WritePix(S.XSPos-Y-TCnt, S.YSPos+X+TCnt, S.LineCol);
					WritePix(S.XSPos+Y+TCnt, S.YSPos-X-TCnt, S.LineCol);
					WritePix(S.XSPos-Y-TCnt, S.YSPos-X-TCnt, S.LineCol);
				}
				break;
			case 3:
				for(TCnt = -1; TCnt<=1; TCnt++){
					WritePix(S.XSPos+X+TCnt, S.YSPos+Y+TCnt, S.LineCol);
					WritePix(S.XSPos-X-TCnt, S.YSPos+Y+TCnt, S.LineCol);
					WritePix(S.XSPos+X+TCnt, S.YSPos-Y-TCnt, S.LineCol);
					WritePix(S.XSPos-X-TCnt, S.YSPos-Y-TCnt, S.LineCol);
					WritePix(S.XSPos+Y+TCnt, S.YSPos+X+TCnt, S.LineCol);
					WritePix(S.XSPos-Y-TCnt, S.YSPos+X+TCnt, S.LineCol);
					WritePix(S.XSPos+Y+TCnt, S.YSPos-X-TCnt, S.LineCol);
					WritePix(S.XSPos-Y-TCnt, S.YSPos-X-TCnt, S.LineCol);
				}
				break;
			}
		}
	}

	return 0;
}


uint32_t Abs(int32_t N){
	if(N>0) return N;
	else return -N;
}



int8_t LineC(ShapeParamStruct S){
	ShapeParamStruct FS;
	int32_t XE = S.XEPos, XS = S.XSPos, YE = S.YEPos, YS = S.YSPos;

	if(Abs(XE-XS)+S.Thickness>XPix-1 || Abs(YE-YS)+S.Thickness>YPix-1 || YE>YPix-1 || XE>XPix-1) return -1;

	int32_t Cnt, TCnt, Distance;
	int32_t XErr = 0, YErr = 0, dX, dY;
	int32_t XInc, YInc, Tck;

	dX = XE-XS;
	dY = YE-YS;

	if(dX>0) XInc = 1;
	else if(dX==0) XInc = 0;
	else XInc = -1;

	if(dY>0) YInc = 1;
	else if(dY==0) YInc = 0;
	else YInc = -1;

	dX = Abs(dX);
	dY = Abs(dY);

	if(dX>dY) Distance = dX;
	else Distance = dY;

	if(S.Thickness>1){
		Tck = S.Thickness>>1;
		FS.XSPos = S.XSPos;
		FS.YSPos = S.YSPos;
		FS.FillCol = S.LineCol;
		FS.Thickness = 0;
		FS.Radius = Tck+1;
		FS.Fill = ENABLE;
		Circle(FS);
		FS.XSPos = S.XEPos;
		FS.YSPos = S.YEPos;
		Circle(FS);
	}

	for(Cnt = 0; Cnt<=Distance+1; Cnt++){
		for(TCnt = 0; TCnt<S.Thickness; TCnt++){
			WritePix(XS, YS+TCnt, S.LineCol);
			WritePix(XS-TCnt, YS, S.LineCol);
			WritePix(XS+TCnt, YS, S.LineCol);
			WritePix(XS, YS-TCnt, S.LineCol);
		}

		XErr+=dX;
		YErr+=dY;
		if(XErr>Distance){
			XErr-=Distance;
			XS+=XInc;
		}
		if(YErr>Distance){
			YErr-=Distance;
			YS+=YInc;
		}
	}
	return 0;
}

int16_t FCos(int16_t Angle){
	return FSin(Angle+450);
}

int16_t FSin(int16_t Angle){
	uint16_t Index;
	int16_t AngRet;

	Angle %= 360;

	Index = (Angle*STLen)/360;

	AngRet = (ST[Index]*Angle + ST[Index+1]*(360-Angle))/360;

	return AngRet;
}

int8_t LineP(ShapeParamStruct S){
//	uint8_t XE, YE;

	S.XEPos = S.XSPos+((S.Radius*FCos(S.Angle))>>12);
	S.YEPos = S.YSPos+((S.Radius*FSin(S.Angle))>>12);
	return LineC(S);
}





virtual int8_t drawBMPfromFlash(uint16_t* ppic, uint16_t size_x, uint16_t size_y){
	int8_t result = -1;
/*
	while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
	while (SPI3->SR & SPI_SR_BSY);
	init_spi3();
 */
	spi.wait_for_bus_free();
	spi.init();
    SB(0x36, Reg); //Set Memory access mode
//#if    		LCD_MODUL_VERSION == 2
	if (LCD_MODUL_VERSION == 2) SB((0x08  |(1<<7)|(1<<6)), Dat);
//#elif 	LCD_MODUL_VERSION == 1
	else  SB((0x08/*|(1<<7)*/), Dat);
//#endif
    SetAddr(0, 0, size_x-1, size_y-1);
/*
    while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
    while (SPI3->SR & SPI_SR_BSY);
 */
    spi.wait_for_bus_free();
    SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, DISABLE);
    DMA_Cmd(DMA2_Channel2, DISABLE);
    /////init_spi3_to_color_lcd();
    spi.init_to_16_bit();
    //LCD_DC1;
    DC.set();
    for(int i=0; i< (uint32_t)size_x*(uint32_t)size_y; i++){

   		////SPI3Send(ppic[i]);
    	spi.Send(ppic[i]);
    	//SW(ppic[i], Dat);
    	//SB(*(p_tmp++),Dat);
   				 }
	return result;
}



virtual int8_t drawBMPfromFlashUseDMA(uint16_t* ppic, uint8_t X, uint8_t Y, uint16_t size_x, uint16_t size_y, TRotation rotation){
	int8_t result = -1;
	 SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, DISABLE);
	 DMA_Cmd(DMA2_Channel2, DISABLE);
/*
	 while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
	 while (SPI3->SR & SPI_SR_BSY);
*/
	 spi.wait_for_bus_free();

//	 if(xSemaphoreTake(xColorLCDSemaphore, (portTickType)(TIC_FQR*2)/*600*/ )== pdTRUE)//если LCD занят, ждем 2 с
//		 {
	 //init_spi3();
	spi.init();
	SB(0x36, Reg); //Set Memory access mode
    switch(rotation){
    	case Rotation_0: {
//#if    		LCD_MODUL_VERSION == 2
  if (LCD_MODUL_VERSION == 2)  		SB((0x08 |(1<<6)|(1<<7)), Dat);
//#elif 	LCD_MODUL_VERSION == 1
  else SB((0x08 /*|(1<<7)*/), Dat);
//#endif

    		SetAddr(X, Y, X+size_x-1, Y+size_y-1);
    	}
    	break;
    	case Rotation_90: {
//#if    		LCD_MODUL_VERSION == 2
   if (LCD_MODUL_VERSION == 2) 		SB((0x08 | (1<<5)/* | (1<<6)*/|(1<<7)), Dat);//меняем местами X и Y, зеркалим по X
//#elif 	LCD_MODUL_VERSION == 1
   else	SB((0x08 | (1<<5) | (1<<6)/*|(1<<7)*/), Dat);//меняем местами X и Y, зеркалим по X
//#endif
    	//	SetAddrForBMP(X, Y, X+size_x-1, Y+size_y-1);
    		//////SetAddrForRotate90(X, Y, X+size_x-1, Y+size_y-1);
    		SetAddrForRotate90(Y, 128-(X+size_y-1), Y+size_x-1, 128-X);
    		//SetAddrForBMP( Y,X, X+size_x-1 ,Y+size_y-1);
    		//SetAddr(X, Y, X+size_x-1, Y+size_y-1);

    	}
    	break;
    	case Rotation_180: {

    	}
    	break;
    	case Rotation_270:{

    	}
    	break;


    }


   // init_spi3_to_color_lcd();
    spi.init_to_16_bit();
    //LCD_DC1;
    DC.set();
    init_dma_to_color_lcd(ppic, (uint32_t)(size_x*size_y));

    xSemaphoreTake(xColorLCD_DMA_Semaphore, (portTickType)(0)/*600*/ );//если LCD занят, ждем 2 с
    	color_lcd_dma_refresh( (uint32_t)(size_x*size_y));
//	vTaskDelay(10);//дадиим время DMA переслать данные
//	xSemaphoreGive(xColorLCDSemaphore);
//		 }
    if(xSemaphoreTake(xColorLCD_DMA_Semaphore, (portTickType)(configTICK_RATE_HZ*2)/*600*/ )== pdTRUE) result=1;//если LCD занят, ждем 2 с
	return result;
}

static void dma_iqr_collback(void){
	static portBASE_TYPE xHigherPriorityTaskWoken;
		xHigherPriorityTaskWoken = pdFALSE;
		   if (DMA_GetFlagStatus(DMA2_FLAG_HT2)==SET)
		   {
			   	DMA_ClearFlag(DMA2_FLAG_HT2);
	//		    xSemaphoreGiveFromISR(xSoundBuffHTSemaphore, &xHigherPriorityTaskWoken );


		   }

		   if (DMA_GetFlagStatus(DMA2_FLAG_TC2)==SET)
		   {
			   	DMA_ClearFlag(DMA2_FLAG_TC2);
	//			DMA_Cmd(DMA2_Channel2, DISABLE);
			    xSemaphoreGiveFromISR(ILI9163<Spi_3>::xColorLCD_DMA_Semaphore, &xHigherPriorityTaskWoken );
		   }

}

static xSemaphoreHandle xColorLCD_DMA_Semaphore;
	SPIx spi;
//private:
		Pin SCE;
protected:

	Pin RST;
	Pin DC;
	Pin BL;



};


template<class SPIx>

xSemaphoreHandle ILI9163<SPIx>::xColorLCD_DMA_Semaphore;








template<class SPIx>
class SSD1283: public ILI9163<SPIx> {
public:
	SSD1283(uint8_t SCE_PORT, uint8_t SCE_PIN,
			uint8_t RST_PORT, uint8_t RST_PIN,
			uint8_t DC_PORT, uint8_t DC_PIN,
			uint8_t BL_PORT, uint8_t BL_PIN
	): ILI9163<SPIx>(SCE_PORT, SCE_PIN,
			RST_PORT, RST_PIN,
			DC_PORT, DC_PIN,
			BL_PORT,BL_PIN){

	}
	TDisplayType type = Reflector;

	constexpr static uint16_t XPix = 130;
	constexpr static uint16_t YPix = 132;
	constexpr static uint16_t X_Offset = 1;
	constexpr static uint16_t Y_Offset = 3;
	constexpr static Colours8 BKGCol = iBlack;//iBlue;
	void init(){

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
		this->spi.init();
		this->reset();
		this->SCE.reset();
		this->bl_on();

		this->SB(0x10,Reg);
		this->SB(0x2F, Dat);
		this->SB(0x8E, Dat);

		this->SB(0x11, Reg);
		this->SB(0x00, Dat);
		this->SB(0x0C, Dat);


		this->SB(0x07, Reg);
		this->SB(0x00, Dat);
		this->SB(0x21, Dat);

		this->SB(0x28, Reg);
		this->SB(0x00, Dat);
		this->SB(0x06, Dat);

		this->SB(0x28, Reg);
		this->SB(0x00, Dat);
		this->SB(0x05, Dat);

		this->SB(0x27, Reg);
		this->SB(0x05, Dat);
		this->SB(0x7F, Dat);

		this->SB(0x29, Reg);
		this->SB(0x89, Dat);
		this->SB(0xA1, Dat);

		this->SB(0x00, Reg);
		this->SB(0x00, Dat);
		this->SB(0x01, Dat);

		this->Delay(100);

		this->SB(0x29, Reg);
		this->SB(0x80, Dat);
		this->SB(0xB0, Dat);

		this->Delay(30);


		this->SB(0x29, Reg); //Set default gamma
		this->SB(0xFF, Dat);
		this->SB(0xFE, Dat);


		this->SB(0x07, Reg); //Set Power Control 1
		this->SB(0x02, Dat);
		this->SB(0x23, Dat);

		this->Delay(30);

		this->SB(0x07,Reg); //Set Power Control 1
		this->SB(0x02, Dat);
		this->SB(0x33, Dat);

		this->SB(0x01, Reg); //Set Power Control 2
		this->SB(0x23, Dat);
		this->SB(0x83, Dat);
/*
		this->SB(0x03, Reg); //Set Power Control 3
		this->SB(0x68, Dat);
		this->SB(0x30, Dat);
*/
		this->setRotate(Rotation_0);


		this->SB(0x2F, Reg); //Set Power Control 4 (Idle mode)
		this->SB(0xFF, Dat);
		this->SB(0xFF, Dat);

		this->SB(0x2C, Reg); //Set VCom Control 1
		this->SB(0x80, Dat); // VComH = 3v
		this->SB(0x00, Dat); // VComL = 0v

		this->SB(0x27, Reg);
		this->SB(0x05, Dat);
		this->SB(0x70, Dat);

		this->SB(0x02, Reg);
		this->SB(0x03, Dat);
		this->SB(0x00, Dat);

		this->SB(0x0B, Reg);
		this->SB(0x58, Dat);
		this->SB(0x0C, Dat);

		this->SB(0x12, Reg);
		this->SB(0x06, Dat);
		this->SB(0x09, Dat);

		this->SB(0x13, Reg);
		this->SB(0x31, Dat);
		this->SB(0x00, Dat);

		this->spi.wait_for_bus_free();

	//	this->SCE.set();



	}

	void SetWindow(int16_t x, int16_t y, int16_t w, int16_t h)
	{
	    uint8_t x0 = (uint8_t)x;
	    uint8_t y0 = (uint8_t)y + 2;
	    uint8_t x1 = (uint8_t)(x + w - 1);
	    uint8_t y1 = (uint8_t)(y + h - 1) + 2;

	    this->SB(0x44,Reg);
	    this->SB(y1, Dat);
	    this->SB(y0, Dat);

	    this->SB(0x45, Reg);
	    this->SB(x1, Dat);
	    this->SB(x0, Dat);

	    this->SB(0x21, Reg);
	    this->SB(x0, Dat);
	    this->SB(y0, Dat);

	    this->SB(0x22, Reg);
	}





	void SetAddr(uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2/*int16_t x, int16_t y, int16_t w, int16_t h*/){




		this->SB(0x44, Reg);
		this->SB(Y2, Dat);
		this->SB(Y1, Dat);

		this->SB(0x45, Reg);
		this->SB(X2, Dat);
		this->SB(X1, Dat);

		this->SB(0x21, Reg);
		this->SB(X1, Dat);
		this->SB(Y1, Dat);

		this->SB(0x22, Reg);


/*
	    uint8_t x0 = (uint8_t)x;
	    uint8_t y0 = (uint8_t)y + 2;
	    uint8_t x1 = (uint8_t)(x + w - 1);
	    uint8_t y1 = (uint8_t)(y + h - 1) + 2;

	    this->SB(0x44, Reg);
	    this->SB(y1, Dat);
	    this->SB(y0, Dat);

	    this->SB(0x45, Reg);
	    this->SB(x1, Dat);
	    this->SB(x0, Dat);

	    this->SB(0x21, Reg);
	    this->SB(x0, Dat);
	    this->SB(y0, Dat);

	    this->SB(0x22, Reg);
*/



	}

	int8_t drawBMPfromFlashUseDMA(uint16_t* ppic, uint8_t X, uint8_t Y, uint16_t size_x, uint16_t size_y, TRotation rotation){

		int8_t result = -1;


	//	uint8_t X_tmp, Y_tmp;

		/*
		X=Y_tmp;
		Y=X_tmp;
*/
		SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, DISABLE);
			 DMA_Cmd(DMA2_Channel2, DISABLE);
			 this->spi.wait_for_bus_free();

			this->spi.init();


			this->setRotate(rotation);

			if(rotation==Rotation_90){
				//Y--;
				//X--;
				//this->SetAddr(Y, XPix-(X+size_y-1), Y+size_x-1, YPix-X);
				this->SetAddr(Y+1, XPix-(X+size_y), Y+size_x, YPix-X+1);
			}

			else
			{
				//Y=Y+Y_Offset;
				//X=X+X_Offset;
				//this->SetAddr(X, Y, X+size_x-1, Y+size_y-1);
				this->SetWindow(X, Y, size_y, size_x );

			}

		    this->spi.init_to_16_bit();
		    //LCD_DC1;
		    this->DC.set();
		    this->init_dma_to_color_lcd(ppic, (uint32_t)(size_x*size_y));

		    xSemaphoreTake(ILI9163<SPIx>::xColorLCD_DMA_Semaphore, (portTickType)(0)/*600*/ );//если LCD занят, ждем 2 с
		    	this->color_lcd_dma_refresh( (uint32_t)(size_x*size_y));

		    if(xSemaphoreTake(ILI9163<SPIx>::xColorLCD_DMA_Semaphore, (portTickType)(configTICK_RATE_HZ*2)/*600*/ )== pdTRUE) result=1;//если LCD занят, ждем 2 с

		    return result;



	}



	int8_t drawBMPfromFlash(uint16_t* ppic, uint16_t size_x, uint16_t size_y){
		int8_t result = -1;

		this->spi.wait_for_bus_free();
		this->spi.init();

	    this->SetAddr(X_Offset, Y_Offset, size_x-1+X_Offset, size_y-1+Y_Offset);

	    this->spi.wait_for_bus_free();
	    SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, DISABLE);
	    DMA_Cmd(DMA2_Channel2, DISABLE);

	    this->spi.init_to_16_bit();
	    //LCD_DC1;
	    this->DC.set();
	    for(int i=0; i< (uint32_t)size_x*(uint32_t)size_y; i++){

	    	this->spi.Send(ppic[i]);
	   				 }
		return result;
	}


void ClrScrn(void){
	/*
	while (!(SPI3->SR & SPI_SR_TXE)); // Wait for bus free
	while (SPI3->SR & SPI_SR_BSY);
	init_spi3();
	*/
	this->spi.wait_for_bus_free();
	this->spi.init();
	this->SCE.reset();
		    //LCD_DC1;
	//this->DC.set();
////	SB(0x36, Reg); //Set Memory access mode


//#if    		LCD_MODUL_VERSION == 2
///if(LCD_MODUL_VERSION == 2)	SB((0x08 |(1<<7)|(1<<6)), Dat);
//#elif 	LCD_MODUL_VERSION == 1
///else	SB((0x08 /*|(1<<7)*/), Dat);
//#endif

	uint16_t XCnt, YCnt;

		SetAddr(0, 0, XPix-1, YPix-1);

		for(XCnt = 0; XCnt<XPix; XCnt++){
			for(YCnt = 0; YCnt<YPix; YCnt++){
				this->SW(BKGCol, Dat);
			}
		}

	//this->SetScrn(BKGCol);
}



void SetAddrForRotate90(uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2){

	X1+=X_Offset;
	X2+=X_Offset;
	Y1+=Y_Offset;
	Y2+=Y_Offset;

	this->SCE.reset();
	this->setRotate(Rotation_90);

	 this->SetAddr(X1, Y1, X2, Y2);
	/*
	SB(0x2A, Reg);
	SB(0x00, Dat);
//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(X1+2, Dat);
//#else
	else SB(32+X1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(X2+2, Dat);
//#else
	else SB(32+X2, Dat);
//#endif

	SB(0x2B, Reg);
	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y1+3, Dat);
//#else
	else SB(Y1, Dat);
//#endif

	SB(0x00, Dat);

//#ifdef LCD_NOT_STANDART
	if(LCD_NOT_STANDART)SB(Y2+3, Dat);
//#else
	else SB(Y2, Dat);
//#endif

	SB(0x2C, Reg);
*/


}


void FillRect(Colours8 Colour, uint8_t X, uint8_t Y, uint8_t size_x, uint8_t size_y){
	uint16_t XCnt, YCnt;
	this->spi.wait_for_bus_free();
	this->spi.init();
	this->setRotate(Rotation_0);
	this->SetWindow(X, Y, size_x, size_y);

	for(XCnt = 0; XCnt<size_x; XCnt++){
				for(YCnt = 0; YCnt<size_y; YCnt++){
					this->SW(Colour, Dat);
				}
			}
}



void setRotate(TRotation rotate){

switch (rotate){
	//this->SCE.reset();
case Rotation_0:
{

	this->SB(0x03,Reg);
	this->SB(0x68, Dat);
	this->SB(0x30, Dat);
	//this->SB(0x28/*0x30&~(1<<4)*/,Dat);
//	this->SB(0x08, Dat);
}
break;

case Rotation_90:
{
	this->SB(0x03,Reg);
	this->SB(0x68, Dat);
	//this->SB((1<<4)|(1<<5),Dat);
	this->SB(0x38, Dat);

}
break;

case Rotation_180:
{
	this->SB(0x03,Reg);
	this->SB(0x68, Dat);
	//this->SB(/*0x08*/0x28,Dat);
	this->SB(0x20, Dat);
}
break;

case Rotation_270:
{
	this->SB(0x03,Reg);
	this->SB(0x68, Dat);
	this->SB((1<<3),Dat);
}
break;
default: break;


}


}


int8_t PChar(uint16_t Ch, TextParamStruct *T){
	uint8_t X = T->XPos, Y = T->YPos, Size = T->Size;
	if(X>XPix-(Size+1)*5 || Y>YPix-(Size+1)*8 || Ch < 0x20) return -1;

	volatile uint8_t XCnt, YCnt;

	Ch-=0x20;
	Ch*=5;
	volatile TRotation rotate = Rotation_90;

	this->setRotate(rotate);

	switch(Size){
	case 0:

		//SetAddr(X, Y, X+4+LetterSpace, Y+7);
//Rotation_0 - развернуто но зеркально

		if(rotate ==Rotation_90)
		{
			this->SetWindow(X, Y, 4+LetterSpace+1, 7+1);



		//this->SetWindow(X, Y, 7, 4+LetterSpace );

		for(YCnt = 0; YCnt<8; YCnt++){
			for(XCnt = 0; XCnt<5+LetterSpace; XCnt++){
				//if(XCnt<5 && (Chars[Ch+XCnt] & (1<<YCnt))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<5 && (Chars[Ch+XCnt] & (1<<YCnt))) this->SW(T->TxtCol, Dat);
				else this->SW(T->BkgCol, Dat);

			}
		}
		}//[if(rotate ==Rotation_90) ]

		else {

			SetWindow(Y, 130-X-5-LetterSpace, 7+1, 4+LetterSpace+1);

			for(YCnt = 0; YCnt<8; YCnt++){
				for(XCnt = 0; XCnt<5+LetterSpace; XCnt++){
					//if(XCnt<5 && (Chars[Ch+XCnt] & (1<<YCnt))) PCol(T->TxtCol);
					//else PCol(T->BkgCol);
					if(XCnt<5 && (Chars[Ch+(4-XCnt)] & (1<<YCnt))) this->SW(T->TxtCol, Dat);
					else this->SW(T->BkgCol, Dat);

				}
			}


		}




		break;
	case 1:
		//SetAddr(X, Y, X+9+LetterSpace, Y+15);

		if(rotate ==Rotation_90)
		{
			this->SetWindow(X, Y, 9+LetterSpace+1, 15+1);


		for(YCnt = 0; YCnt<16; YCnt++){
			for(XCnt = 0; XCnt<10+LetterSpace; XCnt++){
				//if(XCnt<10 && (Chars[Ch+(XCnt>>1)] & (1<<(YCnt>>1)))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<10 && (Chars[Ch+(XCnt>>1)] & (1<<(YCnt>>1)))) this->SW(T->TxtCol, Dat);
				else this->SW(T->BkgCol, Dat);
			}
		}

		}//[if(rotate ==Rotation_90) ]

		else
		{
			this->SetWindow(Y, X/*-10-LetterSpace-2*/, 15+1, 9+LetterSpace+1);
			for(YCnt = 0; YCnt<16; YCnt++){
				for(XCnt = 0; XCnt<10+LetterSpace; XCnt++){
					//if(XCnt<10 && (Chars[Ch+(XCnt>>1)] & (1<<(YCnt>>1)))) PCol(T->TxtCol);
					//else PCol(T->BkgCol);
					if(XCnt<10 && (Chars[Ch+9-(XCnt>>1)] & (1<<(YCnt>>1)))) this->SW(T->TxtCol, Dat);
					else this->SW(T->BkgCol, Dat);
				}
			}




		}

		break;
	case 2:
		//SetAddr(X, Y, X+14+LetterSpace, Y+23);
		if(rotate ==Rotation_90){
			this->SetWindow(X, Y, 14+LetterSpace+1, 23+1);

		for(YCnt = 0; YCnt<24; YCnt++){
			for(XCnt = 0; XCnt<15+LetterSpace; XCnt++){
				//if(XCnt<15 && (Chars[Ch+((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) PCol(T->TxtCol);
				//else PCol(T->BkgCol);
				if(XCnt<15 && (Chars[Ch+((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) this->SW(T->TxtCol, Dat);
				else this->SW(T->BkgCol, Dat);
				}
			}
		}//[if(rotate ==Rotation_90)]

		else
		{
			this->SetWindow(Y, X/*-15-LetterSpace-2*/, 23+1, 14+LetterSpace+1);

			for(YCnt = 0; YCnt<24; YCnt++){
						for(XCnt = 0; XCnt<15+LetterSpace; XCnt++){
							//if(XCnt<15 && (Chars[Ch+((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) PCol(T->TxtCol);
							//else PCol(T->BkgCol);
							if(XCnt<15 && (Chars[Ch+23-((86*XCnt)>>8)] & (1<<((86*YCnt)>>8)))) this->SW(T->TxtCol, Dat);
							else this->SW(T->BkgCol, Dat);
							}
						}


		}


		break;
	}

	if(rotate ==Rotation_90) T->XPos = X+(1+Size)*5+LetterSpace;
	else T->XPos = T->XPos = X+(1+Size)*5+LetterSpace;

	return 0;
}





};









#endif
