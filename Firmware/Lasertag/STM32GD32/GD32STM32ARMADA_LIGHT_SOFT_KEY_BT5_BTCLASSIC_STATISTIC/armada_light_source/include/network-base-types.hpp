#ifndef RF_BASE_TYPES_HPP_
#define RF_BASE_TYPES_HPP_

#include <stdint.h>
#include <functional>
#include <initializer_list>
#include "statistics.h"


using PackageId = uint16_t;



struct DeviceAddress
{
	constexpr static uint8_t size = 3;
	uint8_t address[size];

	DeviceAddress(uint8_t a0 = 1, uint8_t a1 = 1, uint8_t a2 = 1)
		{ address[0] = a0; address[1] = a1, address[2] = a2; }

	void convertFromString(const char* str);

	// Operators
	inline bool operator==(const DeviceAddress& other) const
	{
		for(int i=0; i<size; i++)
			if (address[i] != other.address[i])
				return false;
		return true;
	}

	inline bool operator!=(const DeviceAddress& other) const
	{
		return not (*this == other);
	}

	inline bool operator<(const DeviceAddress& other) const
	{
		for(int i=0; i<size; i++)
		{
			if (address[i] < other.address[i])
				return true;
			if (address[i] > other.address[i])
				return false;
		}
		return false;
	}

	inline DeviceAddress& operator=(const std::initializer_list<uint8_t>& args)
	{
		int i=0;
		for (auto v: args)
		{
			address[i++] = v;
			if (i == 3) break;
		}
		return *this;
	}
};



struct PackageTimings
{
	constexpr static uint32_t defaultTimeout = 10000000;
	constexpr static uint32_t defaultResendTime = 200000;
	constexpr static uint32_t defaultResendTimeDelta = 100000;

	PackageTimings(
			bool _infiniteResend = false,
			uint32_t _timeout = defaultTimeout,
			uint32_t _resendTime = defaultResendTime,
			uint32_t _resendTimeDelta = defaultResendTimeDelta
			) : infiniteResend(_infiniteResend), timeout(_timeout), resendTime(_resendTime), resendTimeDelta(_resendTimeDelta)
	{ }

	bool infiniteResend = false;
	uint32_t timeout = defaultTimeout;
	uint32_t resendTime = defaultResendTime;
	uint32_t resendTimeDelta = defaultResendTimeDelta;
};

using ReceivePackageCallback = std::function<void(DeviceAddress /*sender*/, uint8_t* /*payload*/, uint16_t /*payload length*/)>;
using PackageSendingDoneCallback = std::function<void(PackageId /*package_id*/, bool /*was successfully sent*/)>;



#pragma pack(push, 1)
struct PackageDetails
{
	PackageDetails(uint16_t id, uint8_t ack = 0, uint16_t ttl = 0) :
		packageId(id),
		TTL(ttl),
		needAck(ack)
	{}

	PackageDetails() :
		packageId(0),
		TTL(0),
		needAck(0)
	{
	}

	PackageId packageId;
	uint8_t TTL : 7;
	uint8_t needAck : 1;
};
#pragma pack(pop)



#pragma pack(push, 1)
struct PackageHeader
{
	DeviceAddress sender;
	DeviceAddress target;
	PackageDetails details;
};
#pragma pack(pop)

#pragma pack(push, 1)
struct Package
{
	DeviceAddress sender;
	DeviceAddress target;
	PackageDetails details;

	/// payloadLength is 23 bytes
	constexpr static uint16_t packageLength = 32;//IRadioPhysicalDevice::defaultRFPhysicalPayloadSize;
	constexpr static uint16_t payloadLength /*23*/ = packageLength /*32*/ - sizeof(sender)/*3*/ - sizeof(target)/*3*/ - sizeof(details)/*3*/;

	uint8_t payload[payloadLength];
};
static_assert(sizeof(Package) == Package::packageLength, "Network layer package size is bad");
#pragma pack(pop)



#pragma pack(push, 1)
	struct AckPayload
	{
		constexpr static uint16_t acknoledgementCode = 0xFFFF;
		uint8_t size = sizeof(uint16_t);
		uint16_t operationCode = acknoledgementCode;
		uint16_t packageId;
		bool isAck() { return operationCode == acknoledgementCode; }
	};
#pragma pack(pop)

using Time = portTickType;//uint64_t;

struct WaitingPackage
{
	Time wasCreated = 0;
	Time nextTransmission = 0;
	bool isBroadcast = false;
	PackageTimings timings;
	PackageSendingDoneCallback callback;
	Package package;
};


enum rf_data_type{
	PING=0,
	STATUS_DATA=1,
	NICKNAME=2,
	REQUEST_STATUS=3,
    SET_PARAMETR = 4,
    RESPONSE_TO_COMMAND=5,
	GET_PARAMETR=6,
	RESPONSE_TO_GET_PARAMETR =7,
    START_GAME_COMMAND = 8,
    STOP_GAME_COMMAND=9,
    GET_SYSTEM_SETTINGS =10,
    GET_PLAYER_SETTINGS =11,
    GET_GUN_SETTINGS = 12,
    RESPONSE_TO_GET_SYSTEM_SETTINGS =13,
    RESPONSE_TO_GET_PLAYER_SETTINGS =14,
    RESPONSE_TO_GET_GUN_SETTINGS = 15,
    SET_SYSTEM_SETTINGS = 16,
    SET_PLAYER_SETTINGS = 17,
    SET_GUN_SETTINGS = 18,
	GET_PRESET_SETTINGS = 19,
	RESPONSE_TO_GET_PRESET_SETTINGS = 20,
	SET_PRESET_SETTINGS = 21,
	SET_PLAYER_SETTINGS_BROADCAST =22,
    GET_STATISTICS_BEGIN = 23,
    GET_STATISTICS_NEXT = 24,
    RESPONSE_TO_GET_STATISTICS_BEGIN = 25,
    RESPONSE_TO_GET_STATISTICS_NEXT = 26
};

using trf_data_type = rf_data_type;

enum  parametrCode{
    UNDEFINED=0,
    TEAM_COLOR = 1,
	PLAYER_NICKNAME =2,
	ACTIVE_PRESET = 3
};

using tparametrCode = enum  parametrCode;

#pragma pack(push, 1)
struct hit_confirmation{
	uint8_t need_to_send:1;//нужно ли отправлять подтверждение
	uint8_t in_game :1;//в игре или вне игры
	uint8_t team_color:2; //цвет команды
	uint8_t gun_damage:4;//нанесённый урон
	uint8_t player_id;//идентификатор игрока
};

#pragma pack(pop)


using thit_confirmation = hit_confirmation;


#pragma pack(push, 1)
struct statistics_begin_payload{
	uint8_t type;
	uint8_t team_ID;
	uint8_t player_ID;
	uint16_t shots;
    uint8_t reserve[18] ={0};
};
#pragma pack(pop)

using tstatistics_begin_payload =  statistics_begin_payload;




#pragma pack(push, 1)
struct ComulativeStatisticsPayloadItem {//10
    uint8_t player_ID;          //1
    uint8_t team_ID;            //1
    uint16_t shots=0;           //2
    uint16_t fatal_shorts=0;    //2
    uint32_t damage=0;          //4
    bool isEmpty(void){
        return ((this->shots==0)&&(this->fatal_shorts==0)&&(this->damage==0));
    }

};
#pragma pack(pop)

using ComulativeStatPayloadItem =  ComulativeStatisticsPayloadItem;


#pragma pack(push, 1)//20
struct statistics_payload{
	uint8_t type;								//1
	ComulativeStatPayloadItem stat_items[2]; 	//2*10=20
    uint8_t sender_team_ID;
    uint8_t sender_player_ID;
	//uint8_t reserve[2] ={0};					//2
};
#pragma pack(pop)

using tstatistics_payload =  statistics_payload;


#pragma pack(push, 1)

struct  status_data_payload{//������ 25 ����
	uint8_t type;
	//trf_data_type type; //��� ������������ ������						8 ���
	 uint8_t team_color :2;//���� �������								2 ����
	 uint8_t gun_damage:4;//����, ������� ������� ������				4 ����
	 uint8_t in_game :1;//� ���� ��� ��� ����							1 ���
	 uint8_t bluetooth_is_connected:1;//���������� �� ������-�������	1 ���
	 uint8_t player_id;//������������� ������							8 ���
	 uint8_t health;//��������											8 ���
	 uint16_t battery_voltage;//���������� �� ������� (� ������������)	16 ���
	 uint16_t clips;//���������� �����									16 ���
	 unsigned char nickname[13];//�������								15*8 ���
//	 trx_packet hit_data;//												3*8 ���
	 thit_confirmation  hit_data;//										2*8 ���

};

#pragma pack(pop)

using tstatus_payload = status_data_payload;

#pragma pack(push, 1)
struct gun_settings_payload
{
	uint8_t type; 					//1
	uint8_t damage; 				//1
    uint16_t rounds_placed_in_clip;	//2
   // uint16_t rounds;
    uint16_t clips_after_start;		//2
   // uint16_t clips;
    uint16_t reload_time;			//2
    uint16_t rate;					//2
    uint8_t ir_duty_ctcle;			//1
    uint8_t fire_mode;				//1
    uint8_t reserve[11] ={0};			//11

};
#pragma pack(pop)

using tgun_settings_payload = gun_settings_payload;


#pragma pack(push, 1)
struct player_settings_payload{
 uint8_t type;                  //1
 uint8_t id;                    //1
 uint16_t health_after_start;   //2
 uint8_t shock_time;            //1
 uint16_t safe_time;            //2
 bool zomby;                    //1
 uint8_t zomby_period;          //1
 bool auto_respawn;             //1
 uint16_t auto_respawn_delay;   //2
 uint8_t reserve[11] ={0};      //11
};
#pragma pack(pop)

using tplayer_settings_payload = player_settings_payload;

#pragma pack(push, 1)
struct system_settings_payload{
 uint8_t type;                  //1
 bool friendly_fire;            //1
 bool autostart_game;           //1
 uint8_t backlight_level;       //1
 uint8_t ir_power_mode;         //1
 uint8_t active_preset;         //1
 uint8_t sound_volume;          //1
 uint16_t switching_counter;    //2
 uint16_t full_voltage;			//2
 uint16_t low_voltage;			//2
 uint8_t reserve[10] ={0};      //10
};

#pragma pack(pop)
using tsystem_settings_payload =  system_settings_payload;

#pragma pack(push, 1)
struct preset_settings_payload{
	uint8_t type;						//1			1
	uint16_t health_after_start;		//2			3
	uint16_t clips_after_start;			//2			5
	uint16_t  rounds_placed_in_clip;	//2			7
	uint8_t fire_mode;					//1			8
	uint8_t damage;						//1			9
	uint8_t friendly_fire;				//1			10
	uint8_t shock_time;					//1			11
	uint16_t reload_time;				//2			13
	uint8_t ir_duty_cycle;				//2			15
	uint8_t zomby;						//1			16
	uint8_t zomby_period;				//1			17
	uint16_t rate;						//2			19
	uint8_t preset_number;				//1			20
	uint8_t reserve[3] ={0};      		//3			23
};


#pragma pack(pop)
using tpreset_settings_payload =  preset_settings_payload;

#endif
