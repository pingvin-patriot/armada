#ifndef _POWER_MANAGER_H
#define _POWER_MANAGER_H



#include <stdint.h>
#include <stdlib.h>
#include "pin.h"


#ifdef __cplusplus
 extern "C" {
  #endif
#include "../FreeRTOS/include/FreeRTOS.h"
#include "../FreeRTOS/include/task.h"
#include "../FreeRTOS/include/queue.h"
#include "../FreeRTOS/include/semphr.h"

#ifdef __cplusplus
  }
#endif


class PowerManager/*: public PinIRQ*/ {


public:
//	 void	pin_irq_callback(void) override;
	 void timer_tick(void);
//	 xSemaphoreHandle xSoftKeyOffSemaphore;

PowerManager(uint8_t in_port, uint8_t in_pin, uint8_t out_port, uint8_t out_pin);
void on(uint8_t waiting_time);
void off(void);
bool power_on=false;
protected:
private:
Pin in_pin, out_pin;
//void input_pin_exti(void);

};







#endif
