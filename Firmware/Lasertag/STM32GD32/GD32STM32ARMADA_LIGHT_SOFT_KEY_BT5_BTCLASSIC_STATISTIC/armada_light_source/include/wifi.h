#ifndef __WIFI_H
#define __WIFI_H

#ifdef wifi_enable

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"
#include "pin.h"
#include "usart.h"
#include "parser.h"
#include "project_config.h"


#ifdef __cplusplus
 extern "C" {
  #endif
#include "../FreeRTOS/include/FreeRTOS.h"
#include "../FreeRTOS/include/task.h"
#include "../FreeRTOS/include/queue.h"
#include "../FreeRTOS/include/semphr.h"


#ifdef __cplusplus
  }
#endif



 class Wifi;

class Wifi{

public:
	 Wifi(){};
	 Wifi(char* ssid, char* server_ip, char* port):m_ssid(ssid), m_server_ip(server_ip)
	{
		 self_pointer = this;
	};

private:
	  char* m_ssid;
	  char* m_server_ip;
	  char* m_port;

	  static Wifi* self_pointer;


 };




#endif//[#ifdef wifi]

#endif
