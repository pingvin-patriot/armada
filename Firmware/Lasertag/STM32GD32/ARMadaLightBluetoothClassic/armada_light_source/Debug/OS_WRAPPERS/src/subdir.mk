################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../OS_WRAPPERS/src/os-wrappers.cpp 

OBJS += \
./OS_WRAPPERS/src/os-wrappers.o 

CPP_DEPS += \
./OS_WRAPPERS/src/os-wrappers.d 


# Each subdirectory must supply rules for building sources it contributes
OS_WRAPPERS/src/%.o: ../OS_WRAPPERS/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"C:\Repository_176_212_125_91\ARMADA\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP_MATRYOSHKA_DETECT_BACKLIGHT\armada_light_source\FreeRTOS\include" -I"../OS_WRAPPERS/inc" -I"C:\Repository_176_212_125_91\ARMADA\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP_MATRYOSHKA_DETECT_BACKLIGHT\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


