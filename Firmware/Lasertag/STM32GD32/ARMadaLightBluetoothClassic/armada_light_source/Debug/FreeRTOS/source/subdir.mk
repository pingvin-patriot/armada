################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../FreeRTOS/source/croutine.c \
../FreeRTOS/source/event_groups.c \
../FreeRTOS/source/heap_3.c \
../FreeRTOS/source/list.c \
../FreeRTOS/source/port.c \
../FreeRTOS/source/queue.c \
../FreeRTOS/source/stream_buffer.c \
../FreeRTOS/source/tasks.c \
../FreeRTOS/source/timers.c 

OBJS += \
./FreeRTOS/source/croutine.o \
./FreeRTOS/source/event_groups.o \
./FreeRTOS/source/heap_3.o \
./FreeRTOS/source/list.o \
./FreeRTOS/source/port.o \
./FreeRTOS/source/queue.o \
./FreeRTOS/source/stream_buffer.o \
./FreeRTOS/source/tasks.o \
./FreeRTOS/source/timers.o 

C_DEPS += \
./FreeRTOS/source/croutine.d \
./FreeRTOS/source/event_groups.d \
./FreeRTOS/source/heap_3.d \
./FreeRTOS/source/list.d \
./FreeRTOS/source/port.d \
./FreeRTOS/source/queue.d \
./FreeRTOS/source/stream_buffer.d \
./FreeRTOS/source/tasks.d \
./FreeRTOS/source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
FreeRTOS/source/%.o: ../FreeRTOS/source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"D:\Repository_176_212_125_91\ARMada\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP_MATRYOSHKA_DETECT_BACKLIGHT\armada_light_source\OS_WRAPPERS\inc" -I"D:\Repository_176_212_125_91\ARMada\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP_MATRYOSHKA_DETECT_BACKLIGHT\armada_light_source\FreeRTOS\include" -I"D:\Repository_176_212_125_91\ARMada\FirmWare\LaserTag\GD32STM32\GD32STM32ARMADA_LIGHT_PRESETS_ZOMBY_SND_CNTRL_AUTORESP_MATRYOSHKA_DETECT_BACKLIGHT\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


