################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../OS_WRAPPERS/src/os-wrappers.cpp 

OBJS += \
./OS_WRAPPERS/src/os-wrappers.o 

CPP_DEPS += \
./OS_WRAPPERS/src/os-wrappers.d 


# Each subdirectory must supply rules for building sources it contributes
OS_WRAPPERS/src/%.o: ../OS_WRAPPERS/src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_HD -DGD32F10X_HD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"E:\GitLabRepository\ARMada\armada\Firmware\Lasertag\STM32GD32\ARMadaLightBluetoothClassic\armada_light_source\FreeRTOS\include" -I"E:\GitLabRepository\ARMada\armada\Firmware\Lasertag\STM32GD32\ARMadaLightBluetoothClassic\armada_light_source\OS_WRAPPERS\inc" -I"E:\GitLabRepository\ARMada\armada\Firmware\Lasertag\STM32GD32\ARMadaLightBluetoothClassic\armada_light_source\GD32F10x_standard_peripheral\Include" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


